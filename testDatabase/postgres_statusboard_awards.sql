create schema statusboard;


CREATE EXTENSION
IF NOT EXISTS "uuid-ossp";


create table statusboard.person_types
(
        id serial not null constraint person_types_pkey primary key,
        name varchar(20) not null constraint person_types_name_key unique
);


alter table statusboard.person_types owner to postgres;


INSERT INTO statusboard.person_types
        (name)
VALUES
        ('tech');


INSERT INTO statusboard.person_types
        (name)
VALUES
        ('dev');


INSERT INTO statusboard.person_types
        (name)
VALUES
        ('sales');


INSERT INTO statusboard.person_types
        (name)
VALUES
        ('admin');


create table statusboard.people
(
        id uuid default uuid_generate_v1mc() not null constraint people_pkey primary key,
        name varchar(50) not null,
        handle varchar(20) not null constraint people_handle_key unique,
        title varchar(50),
        is_active boolean,
        type integer constraint people_type_fkey references statusboard.person_types,
        is_geek_of_the_week boolean,
        gitlab_id varchar(255),
        is_geek_of_the_month boolean
);


alter table statusboard.people owner to postgres;


INSERT INTO statusboard.people
        (id, name, handle, title, is_active, type, is_geek_of_the_week, gitlab_id, is_geek_of_the_month)
VALUES
        ('2f25b462-dee3-11e9-ae42-2b66358c63b1',
                'Andrew Ritschel',
                'ARitschel',
                'Operations Lead',
                true,
                1,
                false,
                '',
                false);


INSERT INTO statusboard.people
        (id, name, handle, title, is_active, type, is_geek_of_the_week, gitlab_id, is_geek_of_the_month)
VALUES
        ('f2c3bce4-dee2-11e9-8d5b-abb8311d612e',
                'Todd Stark',
                'TStark',
                'Developer',
                true,
                2,
                false,
                '1329717',
                false);


create table statusboard.awards
(
        id serial not null constraint awards_pkey primary key,
        person uuid constraint awards_person_fkey references statusboard.people,
        name varchar(50) not null,
        date date not null
);


alter table statusboard.awards owner to postgres;

INSERT INTO statusboard.awards
        (person, name, date)
VALUES
        ('f2c3bce4-dee2-11e9-8d5b-abb8311d612e', 'Geek of the Week', '2019-06-10');
INSERT INTO statusboard.awards
        (person, name, date)
VALUES
        ('f2c3bce4-dee2-11e9-8d5b-abb8311d612e', 'Geek of the Week', '2019-09-13');


create table statusboard.current_stats
(
        id serial not null constraint current_stats_pkey primary key,
        person uuid constraint current_stats_person_fkey references statusboard.people,
        customer_satisfaction double precision,
        same_day double precision,
        average_tickets double precision,
        ticket_time double precision,
        last_updated timestamp
);


alter table statusboard.current_stats owner to postgres;

insert into statusboard.current_stats
        (person, customer_satisfaction, same_day, average_tickets, ticket_time, last_updated)
values
        ('2f25b462-dee3-11e9-ae42-2b66358c63b1', 100, 70, 5, 23.86, '2019-09-26 14:37:33.212243');

insert into statusboard.current_stats
        (person, customer_satisfaction, same_day, average_tickets, ticket_time, last_updated)
values
        ('f2c3bce4-dee2-11e9-8d5b-abb8311d612e', 42, 42, 4.2, 42, '2019-09-26 15:13:06.842272');


create table statusboard.stats
(
        id serial not null constraint stats_pkey primary key,
        person uuid constraint stats_person_fkey references statusboard.people,
        date date not null,
        customer_satisfaction double precision,
        same_day double precision,
        average_tickets double precision,
        ticket_time double precision
);


alter table statusboard.stats owner to postgres;

insert into statusboard.stats
        (person, date, customer_satisfaction, same_day, average_tickets, ticket_time)
values
        ('2f25b462-dee3-11e9-ae42-2b66358c63b1', '2019-08-31', 88, 68, 5.142857142857143, 21.98);

insert into statusboard.stats
        (person, date, customer_satisfaction, same_day, average_tickets, ticket_time)
values
        ('2f25b462-dee3-11e9-ae42-2b66358c63b1', '2019-09-25', 100, 87, 6.714285714285714, 37);

