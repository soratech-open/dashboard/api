# Statusboard API 2.0

This repo contains a redesign of the Status Board API.

## Objectives

1. Migrate from MongoDB to PostgreSQL
2. Refactor LDAP integration to allow for more granular permissions
3. Modernize code to better reflect best practices
4. Implement Automated tests