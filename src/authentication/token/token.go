package token

import (
	"errors"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/authentication/ad"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/secret"

	jwt "github.com/dgrijalva/jwt-go"
)

// JwtClaims represent the claims made in a JWT.
type JwtClaims struct {
	Username string   `json:"username"`
	Groups   []string `json:"groups"`
	jwt.StandardClaims
}

// MakeJWT creates a new JSON Web Token with the given user data.
func MakeJWT(data ad.UserData) (string, error) {
	hidden, err := secret.Get()

	if err != nil {
		return "", err
	}

	now := time.Now()
	twelveHours, err := time.ParseDuration("12h")

	if err != nil {
		return "", err
	}

	expires := now.Add(twelveHours)

	userData := JwtClaims{
		data.Username,
		data.Groups,
		jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: expires.Unix(),
			Issuer:    "sora.tech",
			Subject:   data.ID,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, userData)
	return token.SignedString([]byte(hidden.JWT.Key))
}

// ValidateJWT validates the given JWT and returns any claims it contains.
func ValidateJWT(jwtString string) (JwtClaims, error) {
	parsed, err := jwt.ParseWithClaims(jwtString, &JwtClaims{}, func(token *jwt.Token) (interface{}, error) {
		hidden, err := secret.Get()

		if err != nil {
			return false, err
		}

		return []byte(hidden.JWT.Key), nil
	})

	if err != nil {
		return JwtClaims{}, err
	}

	claims := parsed.Claims.(*JwtClaims)

	if !parsed.Valid {
		return JwtClaims{}, errors.New("Token is Invalid")
	}

	if claims.StandardClaims.Valid() != nil {
		return JwtClaims{}, errors.New("Token is Invalid")
	}

	return *claims, nil
}
