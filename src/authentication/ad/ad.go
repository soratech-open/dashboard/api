package ad

import (
	"errors"
	"fmt"
	"regexp"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/secret"

	ldap "gopkg.in/ldap.v2"
)

// UserData represents data about the user.
type UserData struct {
	ID       string   `json:"id"`
	Username string   `json:"username"`
	Groups   []string `json:"groups"`
}

// Login attempts to login using the given credentials.
func Login(username string, password string) (UserData, error) {
	var data UserData

	connection, err := makeLDAPConnection()

	defer connection.Close()

	search := ldap.NewSearchRequest(
		"dc=soratech,dc=office",
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		fmt.Sprintf("(sAMAccountName=%s)", username),
		[]string{"sAMAccountName", "cn", "memberOf"},
		nil,
	)

	request, err := connection.Search(search)

	if err != nil {
		return UserData{}, err
	}

	if len(request.Entries) != 1 {
		return UserData{}, errors.New("User does not exist or too many entries returned")
	}

	userDN := request.Entries[0].DN

	memberOf := request.Entries[0].Attributes[1].Values

	for _, group := range memberOf {
		data.Groups = append(data.Groups, filterGroupName(group))
	}

	err = connection.Bind(userDN, password)

	if err != nil {
		return UserData{}, err
	}

	data.Username = username

	if err != nil {
		return UserData{}, err
	}

	id, err := person.GetIDFromAD(username)

	if err != nil {
		return UserData{}, err
	}

	data.ID = id

	return data, nil
}

func makeLDAPConnection() (*ldap.Conn, error) {
	hidden, err := secret.Get()

	if err != nil {
		return &ldap.Conn{}, err
	}

	connection, err := ldap.Dial("tcp", fmt.Sprintf("%s:%d", hidden.LDAP.Address, hidden.LDAP.Port))

	if err != nil {
		return &ldap.Conn{}, err
	}

	err = connection.Bind(hidden.LDAP.User, hidden.LDAP.Password)

	return connection, err
}

func filterGroupName(group string) string {
	pattern := regexp.MustCompile(`grp\w+`)
	return pattern.FindString(group)
}
