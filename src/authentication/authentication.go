package authentication

import (
	"net/http"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/authentication/ad"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/authentication/token"
)

// LoginData contains the user's data and a jwt cookie.
type LoginData struct {
	User   ad.UserData `json:"user"`
	Cookie http.Cookie
}

// Login attempts to log the user in with the given credentials.
func Login(username string, password string) (LoginData, error) {
	loggedIn := LoginData{}
	data, err := ad.Login(username, password)

	if err != nil {
		return LoginData{}, err
	}

	jwtString, err := token.MakeJWT(data)

	if err != nil {
		return LoginData{}, err
	}

	loggedIn.User = data
	loggedIn.Cookie = makeCookie(jwtString)

	return loggedIn, nil
}

// makeCookie creates an http cookie with the given JWT.
func makeCookie(jwtString string) http.Cookie {
	expiration := time.Now().Add(12 * time.Hour)
	cookie := http.Cookie{
		Name:     "jwt",
		Value:    jwtString,
		Path:     "/",
		Expires:  expiration,
		Secure:   false,
		HttpOnly: true,
	}

	return cookie
}

// HasAccess checks the groups listed in the JWT against the given group.
func HasAccess(r *http.Request, group string) (bool, error) {
	// serverEnv := os.Getenv("SERVER_ENVIRONMENT")

	// if serverEnv == "development" {
	// 	return true, nil
	// }

	jwtString, err := readCookie(r)

	if err != nil {
		return false, err
	}

	if jwtString != "" {
		claims, err := token.ValidateJWT(jwtString)

		if err != nil {
			return false, err
		}

		for _, claimed := range claims.Groups {
			if claimed == group {
				return true, nil
			}
		}
	}

	return false, nil
}

// IsAuthed returns whether the user is logged in or not.
func IsAuthed(r *http.Request) bool {
	jwtString, err := readCookie(r)

	if err != nil {
		return false
	}

	_, err = token.ValidateJWT(jwtString)

	if err != nil {
		return false
	}

	return true
}

// ReadCookie checks the given http.Request for a jwt cookie. If one is found, it returns the contents.
func readCookie(r *http.Request) (string, error) {
	cookie, err := r.Cookie("jwt")

	if err != nil {

		if err.Error() == "http: named cookie not present" {
			return "", nil
		}

		return "", err
	}

	return cookie.Value, nil
}

// IsPerson checks whether the logged in person is the given person.
func IsPerson(r *http.Request, name string) (bool, error) {
	jwtString, err := readCookie(r)

	if err != nil {
		return false, err
	}

	if jwtString == "" {
		return false, nil
	}

	claims, err := token.ValidateJWT(jwtString)

	if err != nil {
		return false, err
	}

	if claims.Username != name {
		return false, nil
	}

	return true, nil
}
