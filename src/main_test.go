package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIndex(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(index)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetByType(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/people/type?type=1", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(peopleHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetActive(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/people/active", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(peopleHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetByID(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/people/person/id?id=2f25b462-dee3-11e9-ae42-2b66358c63b1", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(peopleHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetByHandle(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/people/person/handle?handle=TStark", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(peopleHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetStatsByMonth(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/stats/month?month=9&year=2019", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(statsHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetAllStatsInRange(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/stats/all?start=2019-01-01&end=2019-09-30", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(statsHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetStatsForPersonInRange(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/stats/all?start=2019-01-01&end=2019-09-30&id=2f25b462-dee3-11e9-ae42-2b66358c63b1", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(statsHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetPersonTypes(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/person-type", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(personTypeHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}

func TestGetAwardsFor(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/award?id=f2c3bce4-dee2-11e9-8d5b-abb8311d612e", nil)

	if err != nil {
		t.Error(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(awardHandler)

	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fail()
	}
}
