package persontype

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestCreate(t *testing.T) {
	created, err := Create("test")

	if err != nil {
		t.Error(err)
	}

	if created.ID != 5 || created.Name != "test" {
		t.Fail()
	}
}

func makeTestData() []PersonType {
	return []PersonType{
		PersonType{
			ID:   1,
			Name: "tech",
		},
		PersonType{
			ID:   2,
			Name: "dev",
		},
		PersonType{
			ID:   3,
			Name: "sales",
		},
		PersonType{
			ID:   4,
			Name: "admin",
		},
		PersonType{
			ID:   5,
			Name: "test",
		},
	}
}

func TestGetAll(t *testing.T) {
	testData := makeTestData()

	list, err := GetAll()

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(list, testData) {
		t.Fail()
	}
}
