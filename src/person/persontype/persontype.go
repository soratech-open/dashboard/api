package persontype

import (
	"database/sql"

	// Need this package to communicate with PostgreSQL
	_ "github.com/lib/pq"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/database"
)

// PersonType represents a type of person
type PersonType struct {
	ID   int    `json:"id,omitempty"`
	Name string `json:"name"`
}

// Create adds a new PersonType to the database.
func Create(name string) (PersonType, error) {
	var personType PersonType
	db, err := sql.Open("postgres", database.ConnectionString())

	defer db.Close()

	if err != nil {
		return PersonType{}, err
	}

	err = db.QueryRow("INSERT INTO statusboard.person_types (name) VALUES ($1) RETURNING id, name;", name).Scan(&personType.ID, &personType.Name)

	return personType, err
}

// GetAll returns all the person types in the database.
func GetAll() ([]PersonType, error) {
	var personTypes []PersonType
	db, err := sql.Open("postgres", database.ConnectionString())

	defer db.Close()

	if err != nil {
		return []PersonType{}, err
	}

	rows, err := db.Query("SELECT * FROM statusboard.person_types")

	if err != nil {
		return []PersonType{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var pt PersonType

		err = rows.Scan(&pt.ID, &pt.Name)

		if err != nil {
			return []PersonType{}, err
		}

		personTypes = append(personTypes, pt)
	}

	return personTypes, rows.Err()
}
