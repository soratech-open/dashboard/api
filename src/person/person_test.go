package person

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func makeTestData() []Person {
	return []Person{
		Person{
			ID:             "2f25b462-dee3-11e9-ae42-2b66358c63b1",
			Name:           "Andrew Ritschel",
			Handle:         "ARitschel",
			Title:          "Operations Lead",
			Active:         true,
			Type:           1,
			GeekOfTheWeek:  false,
			GitlabID:       "",
			GeekOfTheMonth: false,
		},
		Person{
			ID:             "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
			Name:           "Todd Stark",
			Handle:         "TStark",
			Title:          "Developer",
			Active:         true,
			Type:           2,
			GeekOfTheWeek:  false,
			GitlabID:       "1329717",
			GeekOfTheMonth: false,
		},
	}
}

func TestGetAll(t *testing.T) {
	testData := makeTestData()

	people, err := GetAll()

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(testData, people) {
		t.Fail()
	}
}

func TestGetByID(t *testing.T) {
	testData := makeTestData()

	todd, err := GetByID("f2c3bce4-dee2-11e9-8d5b-abb8311d612e")

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(testData[1], todd) {
		t.Fail()
	}
}

func TestGetByHandle(t *testing.T) {
	testData := makeTestData()

	andrew, err := GetByHandle("ARitschel")

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(andrew, testData[0]) {
		t.Fail()
	}
}

func TestGetByType(t *testing.T) {
	testData := makeTestData()

	testSlice := make([]Person, 1)

	testSlice[0] = testData[0]

	techs, err := GetByType(1)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(techs, testSlice) {
		t.Fail()
	}
}

func TestGetActive(t *testing.T) {
	testData := makeTestData()

	active, err := GetActive(true)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(active, testData) {
		t.Fail()
	}
}

func TestCreate(t *testing.T) {
	newPerson := Person{
		Name:          "Test Person",
		Handle:        "TPerson",
		Title:         "Simply a test",
		Active:        true,
		Type:          4,
		GeekOfTheWeek: false,
	}

	created, err := Create(newPerson)

	if err != nil {
		t.Error(err)
	}

	// can't use cmp.Equal() here because we can't predict the new users UUID.
	switch {
	case created.Name != newPerson.Name:
		fallthrough
	case created.Handle != newPerson.Handle:
		fallthrough
	case created.Title != newPerson.Title:
		fallthrough
	case created.Active != newPerson.Active:
		fallthrough
	case created.Type != newPerson.Type:
		fallthrough
	case created.GeekOfTheWeek != newPerson.GeekOfTheWeek:
		t.Fail()
	}
}

func TestUpdate(t *testing.T) {
	updatedTodd := Person{
		ID:            "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
		Name:          "Todd Stark II",
		Handle:        "TStark",
		Title:         "Software Engineer",
		Active:        true,
		Type:          2,
		GeekOfTheWeek: true,
		GitlabID:      "1329717",
	}

	updated, err := Update(updatedTodd)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(updated, updatedTodd) {
		t.Fail()
	}
}

func TestSetGeekOfTheWeek(t *testing.T) {
	err := SetGeekOfTheWeek("f2c3bce4-dee2-11e9-8d5b-abb8311d612e")

	if err != nil {
		t.Error(err)
	}
}

func TestGetGeekOfTheWeek(t *testing.T) {
	updatedTodd := Person{
		ID:            "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
		Name:          "Todd Stark II",
		Handle:        "TStark",
		Title:         "Software Engineer",
		Active:        true,
		Type:          2,
		GeekOfTheWeek: true,
		GitlabID:      "1329717",
	}

	gotw, err := GetGeekOfTheWeek()

	if err != nil {
		t.Error(err)
	}

	unsetCurrentGotw()

	if !cmp.Equal(gotw, updatedTodd) {
		t.Fail()
	}
}

func TestGetIDFromAD(t *testing.T) {
	desiredID := "f2c3bce4-dee2-11e9-8d5b-abb8311d612e"

	id, err := GetIDFromAD("tstark")

	if err != nil {
		t.Error(err)
	}

	if id != desiredID {
		t.Fail()
	}
}

func TestSetGeekOfTheMonth(t *testing.T) {
	err := SetGeekOfTheMonth("f2c3bce4-dee2-11e9-8d5b-abb8311d612e")

	if err != nil {
		t.Error(err)
	}
}

func TestGetGeekOfTheMonth(t *testing.T) {
	updatedTodd := Person{
		ID:             "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
		Name:           "Todd Stark II",
		Handle:         "TStark",
		Title:          "Software Engineer",
		Active:         true,
		Type:           2,
		GeekOfTheWeek:  false,
		GitlabID:       "1329717",
		GeekOfTheMonth: true,
	}

	gotm, err := GetGeekOfTheMonth()

	if err != nil {
		t.Error(err)
	}

	unsetCurrentGotm()

	if !cmp.Equal(gotm, updatedTodd) {
		t.Fail()
	}
}
