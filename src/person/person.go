package person

import (
	"database/sql"
	// Need this package to communicate with PostgreSQL
	_ "github.com/lib/pq"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/database"
)

// Person represents a single person in the database.
type Person struct {
	ID             string `json:"id,omitempty"`
	Name           string `json:"name,omitempty"`
	Handle         string `json:"handle,omitempty"`
	Title          string `json:"title,omitempty"`
	Active         bool   `json:"isActive,omitempty"`
	Type           int    `json:"type,omitempty"`
	GeekOfTheWeek  bool   `json:"isGeekOfTheWeek,omitempty"`
	GitlabID       string `json:"gitlabID,omitempty"`
	GeekOfTheMonth bool   `json:"isGeekOfTheMonth,omitempty"`
}

// Create adds a new person to the database.
func Create(person Person) (Person, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Person{}, err
	}

	defer db.Close()

	row := db.QueryRow(`
		INSERT INTO statusboard.people (name, handle, title, is_active, type, is_geek_of_the_week, gitlab_id, is_geek_of_the_month)
		VALUES ($1, $2, $3, $4, $5, $6, $7, false)
		RETURNING id;
	`, person.Name, person.Handle, person.Title, person.Active, person.Type, false, person.GitlabID)

	err = row.Scan(&person.ID)

	return person, err
}

// GetAll returns all people in the database.
func GetAll() ([]Person, error) {
	var people []Person
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Person{}, err
	}

	defer db.Close()

	rows, err := db.Query("SELECT * FROM statusboard.people;")

	if err != nil {
		return []Person{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Person

		err = rows.Scan(
			&scanned.ID,
			&scanned.Name,
			&scanned.Handle,
			&scanned.Title,
			&scanned.Active,
			&scanned.Type,
			&scanned.GeekOfTheWeek,
			&scanned.GitlabID,
			&scanned.GeekOfTheMonth,
		)

		if err != nil {
			return []Person{}, err
		}

		people = append(people, scanned)
	}

	return people, rows.Err()
}

// GetByID returns the person in the database with the given ID.
func GetByID(id string) (Person, error) {
	var person Person
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Person{}, err
	}

	defer db.Close()

	row := db.QueryRow("SELECT * FROM statusboard.people WHERE id = $1", id)

	err = row.Scan(
		&person.ID,
		&person.Name,
		&person.Handle,
		&person.Title,
		&person.Active,
		&person.Type,
		&person.GeekOfTheWeek,
		&person.GitlabID,
		&person.GeekOfTheMonth,
	)

	return person, err
}

// GetByHandle returns the person in the database with the given Handle.
func GetByHandle(handle string) (Person, error) {
	var person Person
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Person{}, err
	}

	defer db.Close()

	row := db.QueryRow("SELECT * FROM statusboard.people WHERE handle = $1", handle)

	err = row.Scan(
		&person.ID,
		&person.Name,
		&person.Handle,
		&person.Title,
		&person.Active,
		&person.Type,
		&person.GeekOfTheWeek,
		&person.GitlabID,
		&person.GeekOfTheMonth,
	)

	return person, err
}

// Update modifies the given person in the database.
func Update(person Person) (Person, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Person{}, err
	}

	defer db.Close()

	statement, err := db.Prepare(`
		UPDATE statusboard.people
		SET name = $1,
			handle = $2,
			title = $3,
			is_active = $4,
			type = $5,
			gitlab_id = $6
		WHERE id = $7;
	`)

	if err != nil {
		return Person{}, err
	}

	_, err = statement.Exec(person.Name, person.Handle, person.Title, person.Active, person.Type, person.GitlabID, person.ID)

	return person, err
}

// GetByType returns all people with the given type.
func GetByType(pType int) ([]Person, error) {
	var people []Person

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Person{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.people
		WHERE type = $1
		AND is_active = true
		ORDER BY name ASC;
	`, pType)

	if err != nil {
		return []Person{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Person

		err = rows.Scan(
			&scanned.ID,
			&scanned.Name,
			&scanned.Handle,
			&scanned.Title,
			&scanned.Active,
			&scanned.Type,
			&scanned.GeekOfTheWeek,
			&scanned.GitlabID,
			&scanned.GeekOfTheMonth,
		)

		if err != nil {
			return []Person{}, err
		}

		people = append(people, scanned)
	}

	return people, rows.Err()
}

// GetActive returns all people with an is_active flag matching the given boolean.
func GetActive(active bool) ([]Person, error) {
	var people []Person

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Person{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.people
		WHERE is_active = $1
		AND type != 5
		ORDER BY name ASC;
	`, active)

	if err != nil {
		return []Person{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Person

		err = rows.Scan(
			&scanned.ID,
			&scanned.Name,
			&scanned.Handle,
			&scanned.Title,
			&scanned.Active,
			&scanned.Type,
			&scanned.GeekOfTheWeek,
			&scanned.GitlabID,
			&scanned.GeekOfTheMonth,
		)

		if err != nil {
			return []Person{}, err
		}

		people = append(people, scanned)
	}

	return people, rows.Err()
}

// GetGeekOfTheWeek returns the current Geek of the Week.
func GetGeekOfTheWeek() (Person, error) {
	var gotw Person

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Person{}, err
	}

	defer db.Close()

	row := db.QueryRow("SELECT * FROM statusboard.people WHERE is_geek_of_the_week = true")

	err = row.Scan(
		&gotw.ID,
		&gotw.Name,
		&gotw.Handle,
		&gotw.Title,
		&gotw.Active,
		&gotw.Type,
		&gotw.GeekOfTheWeek,
		&gotw.GitlabID,
		&gotw.GeekOfTheMonth,
	)

	return gotw, err
}

// SetGeekOfTheWeek set the given person to be geek of the week.
func SetGeekOfTheWeek(id string) error {
	err := unsetCurrentGotw()

	if err != nil {
		return err
	}

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return err
	}

	defer db.Close()

	statement, err := db.Prepare(`
		UPDATE statusboard.people
		SET is_geek_of_the_week = true
		WHERE id = $1
	`)

	if err != nil {
		return err
	}

	_, err = statement.Exec(id)

	return err
}

// unsetCurrentGotw unsets the current Geek of the Week.
func unsetCurrentGotw() error {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return err
	}

	defer db.Close()

	statement, err := db.Prepare(`
		UPDATE statusboard.people
		SET is_geek_of_the_week = false
		WHERE is_geek_of_the_week = true
	`)

	if err != nil {
		return err
	}

	_, err = statement.Exec()

	return err
}

// GetIDFromAD returns the ID of the user with the given AD name.
func GetIDFromAD(adName string) (string, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return "", err
	}

	defer db.Close()

	var id string

	err = db.QueryRow("SELECT id FROM statusboard.people WHERE LOWER(handle) = $1", adName).Scan(&id)

	return id, err
}

// GetGeekOfTheMonth returns the current Geek of the Month.
func GetGeekOfTheMonth() (Person, error) {
	var gotm Person

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Person{}, err
	}

	defer db.Close()

	row := db.QueryRow("SELECT * FROM statusboard.people WHERE is_geek_of_the_month = true")

	err = row.Scan(
		&gotm.ID,
		&gotm.Name,
		&gotm.Handle,
		&gotm.Title,
		&gotm.Active,
		&gotm.Type,
		&gotm.GeekOfTheWeek,
		&gotm.GitlabID,
		&gotm.GeekOfTheMonth,
	)

	return gotm, err
}

// SetGeekOfTheMonth set the given person to be geek of the month.
func SetGeekOfTheMonth(id string) error {
	err := unsetCurrentGotm()

	if err != nil {
		return err
	}

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return err
	}

	defer db.Close()

	statement, err := db.Prepare(`
		UPDATE statusboard.people
		SET is_geek_of_the_month = true
		WHERE id = $1
	`)

	if err != nil {
		return err
	}

	_, err = statement.Exec(id)

	return err
}

// unsetCurrentGotw unsets the current Geek of the Week.
func unsetCurrentGotm() error {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return err
	}

	defer db.Close()

	statement, err := db.Prepare(`
		UPDATE statusboard.people
		SET is_geek_of_the_month = false
		WHERE is_geek_of_the_month = true
	`)

	if err != nil {
		return err
	}

	_, err = statement.Exec()

	return err
}
