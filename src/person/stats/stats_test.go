package stats

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"
)

func makeTestData() []Record {
	list := make([]Record, 2)

	list[0] = Record{
		ID: 1,
		Person: person.Person{
			ID:             "2f25b462-dee3-11e9-ae42-2b66358c63b1",
			Name:           "Andrew Ritschel",
			Handle:         "ARitschel",
			Title:          "Operations Lead",
			Active:         true,
			Type:           1,
			GeekOfTheWeek:  false,
			GitlabID:       "",
			GeekOfTheMonth: false,
		},
		Date: "2019-08-31",
		Stats: Stats{
			CustomerSatisfaction: 88,
			SameDay:              68,
			AverageTickets:       5.142857142857143,
			TicketTime:           21.98,
		},
	}

	list[1] = Record{
		ID: 2,
		Person: person.Person{
			ID:             "2f25b462-dee3-11e9-ae42-2b66358c63b1",
			Name:           "Andrew Ritschel",
			Handle:         "ARitschel",
			Title:          "Operations Lead",
			Active:         true,
			Type:           1,
			GeekOfTheWeek:  false,
			GitlabID:       "",
			GeekOfTheMonth: false,
		},
		Date: "2019-09-25",
		Stats: Stats{
			CustomerSatisfaction: 100,
			SameDay:              87,
			AverageTickets:       6.714285714285714,
			TicketTime:           37,
		},
	}

	return list
}

func TestGetForMonth(t *testing.T) {
	testData := makeTestData()

	testSlice := make([]Record, 1)

	testSlice[0] = testData[0]

	august, err := GetForMonth(2019, 8)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(august, testSlice) {
		t.Fail()
	}
}

func TestGetForDateRange(t *testing.T) {
	testData := makeTestData()

	stats, err := GetForDateRange("2019-01-01", "2019-12-31")

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(stats, testData) {
		t.Fail()
	}
}

func TestGetForPersonInRange(t *testing.T) {
	testData := makeTestData()

	stats, err := GetForPersonInRange("2f25b462-dee3-11e9-ae42-2b66358c63b1", "2019-01-01", "2019-12-31")

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(stats, testData) {
		t.Fail()
	}
}

func TestCreate(t *testing.T) {
	testStats := Record{
		ID: 3,
		Person: person.Person{
			ID: "2f25b462-dee3-11e9-ae42-2b66358c63b1",
		},
		Date: "2019-07-31",
		Stats: Stats{
			CustomerSatisfaction: 91.4,
			SameDay:              74,
			AverageTickets:       6.739130434782608,
			TicketTime:           31.69,
		},
	}

	created, err := Add(testStats)

	if err != nil {
		t.Fail()
	}

	if !cmp.Equal(created, testStats) {
		t.Fail()
	}
}

func TestUpdate(t *testing.T) {
	testStats := Record{
		ID: 3,
		Person: person.Person{
			ID: "2f25b462-dee3-11e9-ae42-2b66358c63b1",
		},
		Date: "2019-07-31",
		Stats: Stats{
			CustomerSatisfaction: 42,
			SameDay:              42,
			AverageTickets:       4.2,
			TicketTime:           42,
		},
	}

	created, err := Add(testStats)

	if err != nil {
		t.Fail()
	}

	if !cmp.Equal(created, testStats) {
		t.Fail()
	}
}
