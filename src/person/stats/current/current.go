package current

import (
	"database/sql"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/stats"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/database"

	// Need this package to communicate with PostgreSQL
	_ "github.com/lib/pq"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"
)

// Record represents one record in the current_stats table.
type Record struct {
	ID          int           `json:"id"`
	Person      person.Person `json:"person"`
	Stats       stats.Stats   `json:"stats"`
	LastUpdated time.Time     `json:"lastUpdated"`
}

// create adds a new record to the database.
func create(stats Record) (Record, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Record{}, err
	}

	defer db.Close()

	err = db.QueryRow(`
		INSERT INTO statusboard.current_stats(person, customer_satisfaction, same_day, average_tickets, ticket_time, last_updated)
		VALUES ($1, $2, $3, $4, $5, $6)
		RETURNING id;
	`, stats.Person.ID, stats.Stats.CustomerSatisfaction, stats.Stats.SameDay, stats.Stats.AverageTickets, stats.Stats.TicketTime, stats.LastUpdated).Scan(&stats.ID)

	return stats, err
}

// update modifies the given person's current stat record.
func update(stats Record) (Record, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Record{}, err
	}

	defer db.Close()

	statement, err := db.Prepare(`
		UPDATE statusboard.current_stats
		SET customer_satisfaction = $1,
		same_day = $2,
		average_tickets = $3,
		ticket_time = $4,
		last_updated = $5
		WHERE person = $6
	`)

	if err != nil {
		return Record{}, err
	}

	_, err = statement.Exec(stats.Stats.CustomerSatisfaction, stats.Stats.SameDay, stats.Stats.AverageTickets, stats.Stats.TicketTime, time.Now(), stats.Person.ID)

	return stats, err
}

// Add either creates a new stat record if the given user doesn't have one, or updates it if he or she does.
func Add(stats Record) (Record, error) {
	exists, err := database.RowExists("SELECT * FROM statusboard.current_stats WHERE person = $1", stats.Person.ID)

	if err != nil {
		return Record{}, err
	}

	if !exists {
		return create(stats)
	}

	return update(stats)
}

// GetFor returns the current stats for the given person.
func GetFor(person string) (Record, error) {
	var data Record
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Record{}, err
	}

	defer db.Close()

	row := db.QueryRow(`
		SELECT * FROM statusboard.current_stats c
		JOIN statusboard.people p ON c.person = p.id
		WHERE c.person = $1;
	`, person)

	err = row.Scan(
		&data.ID,
		&data.Person.ID,
		&data.Stats.CustomerSatisfaction,
		&data.Stats.SameDay,
		&data.Stats.AverageTickets,
		&data.Stats.TicketTime,
		&data.LastUpdated,
		&data.Person.ID,
		&data.Person.Name,
		&data.Person.Handle,
		&data.Person.Title,
		&data.Person.Active,
		&data.Person.Type,
		&data.Person.GeekOfTheWeek,
		&data.Person.GitlabID,
		&data.Person.GeekOfTheMonth,
	)

	return data, err
}

// GetForType returns the current stats for all people of the given type.
func GetForType(personType int) ([]Record, error) {
	var list []Record

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Record{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.current_stats c
		JOIN statusboard.people p ON c.person = p.id
		WHERE p.type = $1
		AND p.is_active = true
		ORDER BY p.name ASC;
	`, personType)

	if err != nil {
		return []Record{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Record

		err = rows.Scan(
			&scanned.ID,
			&scanned.Person.ID,
			&scanned.Stats.CustomerSatisfaction,
			&scanned.Stats.SameDay,
			&scanned.Stats.AverageTickets,
			&scanned.Stats.TicketTime,
			&scanned.LastUpdated,
			&scanned.Person.ID,
			&scanned.Person.Name,
			&scanned.Person.Handle,
			&scanned.Person.Title,
			&scanned.Person.Active,
			&scanned.Person.Type,
			&scanned.Person.GeekOfTheWeek,
			&scanned.Person.GitlabID,
			&scanned.Person.GeekOfTheMonth,
		)

		list = append(list, scanned)
	}

	return list, rows.Err()
}

// GetAll returns all current stats.
func GetAll() ([]Record, error) {
	var list []Record

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Record{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.current_stats c
		JOIN statusboard.people p ON c.person = p.id;
	`)

	if err != nil {
		return []Record{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Record

		err = rows.Scan(
			&scanned.ID,
			&scanned.Person.ID,
			&scanned.Stats.CustomerSatisfaction,
			&scanned.Stats.SameDay,
			&scanned.Stats.AverageTickets,
			&scanned.Stats.TicketTime,
			&scanned.LastUpdated,
			&scanned.Person.ID,
			&scanned.Person.Name,
			&scanned.Person.Handle,
			&scanned.Person.Title,
			&scanned.Person.Active,
			&scanned.Person.Type,
			&scanned.Person.GeekOfTheWeek,
			&scanned.Person.GitlabID,
			&scanned.Person.GeekOfTheMonth,
		)

		list = append(list, scanned)
	}

	return list, rows.Err()
}
