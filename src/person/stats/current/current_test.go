package current

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/stats"
)

func makeTestData() []Record {
	andrewTime, _ := time.Parse("2006-01-02 15:04:05.000000", "2019-09-26 14:37:33.212243")
	toddTime, _ := time.Parse("2006-01-02 15:04:05.000000", "2019-09-26 15:13:06.842272")

	return []Record{
		Record{
			ID: 1,
			Person: person.Person{
				ID:             "2f25b462-dee3-11e9-ae42-2b66358c63b1",
				Name:           "Andrew Ritschel",
				Handle:         "ARitschel",
				Title:          "Operations Lead",
				Active:         true,
				Type:           1,
				GeekOfTheWeek:  false,
				GitlabID:       "",
				GeekOfTheMonth: false,
			},
			Stats: stats.Stats{
				CustomerSatisfaction: 100,
				SameDay:              70,
				AverageTickets:       5,
				TicketTime:           23.86,
			},
			LastUpdated: andrewTime,
		},
		Record{
			ID: 2,
			Person: person.Person{
				ID:             "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
				Name:           "Todd Stark II",
				Handle:         "TStark",
				Title:          "Software Engineer",
				Active:         true,
				Type:           2,
				GeekOfTheWeek:  false,
				GitlabID:       "1329717",
				GeekOfTheMonth: false,
			},
			Stats: stats.Stats{
				CustomerSatisfaction: 42,
				SameDay:              42,
				AverageTickets:       4.2,
				TicketTime:           42,
			},
			LastUpdated: toddTime,
		},
	}
}

func TestGetFor(t *testing.T) {
	testData := makeTestData()

	toddStats, err := GetFor("f2c3bce4-dee2-11e9-8d5b-abb8311d612e")

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(toddStats, testData[1]) {
		fmt.Printf("Expected: %v\n", testData[1])
		fmt.Printf("Result: %v\n", toddStats)
		t.Fail()
	}
}

func TestGetForType(t *testing.T) {
	testData := makeTestData()
	testSlice := make([]Record, 1)
	testSlice[0] = testData[0]

	techStats, err := GetForType(1)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(techStats, testSlice) {
		t.Fail()
	}
}

func TestGetAll(t *testing.T) {
	testData := makeTestData()

	allStats, err := GetAll()

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(allStats, testData) {
		t.Fail()
	}
}

func TestUpdate(t *testing.T) {
	updateTime := time.Now()
	newData := Record{
		ID: 2,
		Person: person.Person{
			ID: "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
		},
		Stats: stats.Stats{
			CustomerSatisfaction: 42.42,
			SameDay:              42.42,
			AverageTickets:       4.242,
			TicketTime:           42.42,
		},
		LastUpdated: updateTime,
	}

	updated, err := Add(newData)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(updated, newData) {
		t.Fail()
	}
}
