package stats

import (
	"database/sql"
	"strings"
	"time"

	// Need this package to communicate with PostgreSQL
	_ "github.com/lib/pq"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/database"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"
)

// Record represents a single month's statistics for a single person.
type Record struct {
	ID     int           `json:"id"`
	Person person.Person `json:"person"`
	Date   string        `json:"date"`
	Stats  Stats         `json:"stats"`
}

// Stats represents the actual statistics to be tracked.
type Stats struct {
	CustomerSatisfaction float64 `json:"customerSatisfaction"`
	SameDay              float64 `json:"sameDay"`
	AverageTickets       float64 `json:"averageTickets"`
	TicketTime           float64 `json:"ticketTime"`
}

// GetForMonth returns all the Statistic for the given month and year.
func GetForMonth(year int, month int) ([]Record, error) {
	var list []Record

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Record{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.stats s
		JOIN statusboard.people p ON s.person = p.id
		WHERE EXTRACT(MONTH FROM s.date) = $1
		AND EXTRACT(YEAR FROM s.date) = $2;
	`, month, year)

	if err != nil {
		return []Record{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Record
		var scannedDate time.Time

		err := rows.Scan(
			&scanned.ID,
			&scanned.Person.ID,
			&scannedDate,
			&scanned.Stats.CustomerSatisfaction,
			&scanned.Stats.SameDay,
			&scanned.Stats.AverageTickets,
			&scanned.Stats.TicketTime,
			&scanned.Person.ID,
			&scanned.Person.Name,
			&scanned.Person.Handle,
			&scanned.Person.Title,
			&scanned.Person.Active,
			&scanned.Person.Type,
			&scanned.Person.GeekOfTheWeek,
			&scanned.Person.GitlabID,
			&scanned.Person.GeekOfTheMonth,
		)

		if err != nil {
			return []Record{}, err
		}

		scanned.Date = scannedDate.Format("2006-01-02")

		list = append(list, scanned)
	}

	return list, rows.Err()
}

// GetForDateRange returns all Statistic that fall within the given date range.
func GetForDateRange(start string, end string) ([]Record, error) {
	var list []Record
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Record{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.stats s
		JOIN statusboard.people p ON s.person = p.id
		WHERE s.date BETWEEN $1 AND $2
		AND p.type NOT IN (2,3,4)
		ORDER BY s.date ASC;
	`, start, end)

	if err != nil {
		return []Record{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Record
		var scannedDate time.Time

		err = rows.Scan(
			&scanned.ID,
			&scanned.Person.ID,
			&scannedDate,
			&scanned.Stats.CustomerSatisfaction,
			&scanned.Stats.SameDay,
			&scanned.Stats.AverageTickets,
			&scanned.Stats.TicketTime,
			&scanned.Person.ID,
			&scanned.Person.Name,
			&scanned.Person.Handle,
			&scanned.Person.Title,
			&scanned.Person.Active,
			&scanned.Person.Type,
			&scanned.Person.GeekOfTheWeek,
			&scanned.Person.GitlabID,
			&scanned.Person.GeekOfTheMonth,
		)

		if err != nil {
			return []Record{}, err
		}

		scanned.Date = scannedDate.Format("2006-01-02")

		list = append(list, scanned)
	}

	return list, rows.Err()
}

// GetForPersonInRange gets all stats for the given person in the given date range.
func GetForPersonInRange(id string, start string, end string) ([]Record, error) {
	var list []Record
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Record{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.stats s
		JOIN statusboard.people p ON s.person = p.id
		WHERE s.person = $1
		AND s.date BETWEEN $2 AND $3
		ORDER BY s.date ASC;
	`, id, start, end)

	if err != nil {
		return []Record{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Record
		var scannedDate time.Time

		err = rows.Scan(
			&scanned.ID,
			&scanned.Person.ID,
			&scannedDate,
			&scanned.Stats.CustomerSatisfaction,
			&scanned.Stats.SameDay,
			&scanned.Stats.AverageTickets,
			&scanned.Stats.TicketTime,
			&scanned.Person.ID,
			&scanned.Person.Name,
			&scanned.Person.Handle,
			&scanned.Person.Title,
			&scanned.Person.Active,
			&scanned.Person.Type,
			&scanned.Person.GeekOfTheWeek,
			&scanned.Person.GitlabID,
			&scanned.Person.GeekOfTheMonth,
		)

		if err != nil {
			return []Record{}, err
		}

		scanned.Date = scannedDate.Format("2006-01-02")

		list = append(list, scanned)
	}

	return list, rows.Err()
}

// update modifies the given stat object in the database.
func update(stats Record) (Record, error) {
	parsedDate, err := time.Parse("2006-01-02", stats.Date)

	if err != nil {
		return Record{}, err
	}

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Record{}, err
	}

	defer db.Close()

	err = db.QueryRow(`
		UPDATE statusboard.stats
		SET date = $1,
		customer_satisfaction = $2,
		same_day = $3,
		average_tickets = $4,
		ticket_time = $5
		WHERE person = $6
		AND EXTRACT(YEAR FROM date) = $7
		AND EXTRACT(MONTH FROM date) = $8
		RETURNING id;
	`, stats.Date, stats.Stats.CustomerSatisfaction, stats.Stats.SameDay, stats.Stats.AverageTickets, stats.Stats.TicketTime, stats.Person.ID, parsedDate.Year(), int(parsedDate.Month())).Scan(&stats.ID)

	return stats, err
}

// create adds a new Statistic object to the database.
func create(stats Record) (Record, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Record{}, err
	}

	defer db.Close()

	row := db.QueryRow(`
		INSERT INTO statusboard.stats(
			person,
			date,
			customer_satisfaction,
			same_day,
			average_tickets,
			ticket_time
		) VALUES ($1, $2, $3, $4, $5, $6)
		RETURNING id;
	`, stats.Person.ID, stats.Date, stats.Stats.CustomerSatisfaction, stats.Stats.SameDay, stats.Stats.AverageTickets, stats.Stats.TicketTime)

	err = row.Scan(&stats.ID)

	return stats, err
}

// Add creates a new stat record if none exists, otherwise it updates the existing one.
func Add(stats Record) (Record, error) {
	parsedDate, err := time.Parse("2006-01-02", stats.Date)

	if err != nil {
		return Record{}, err
	}

	exists, err := database.RowExists(`
		SELECT * FROM statusboard.stats 
		WHERE EXTRACT(YEAR FROM date) = $1 
		AND EXTRACT(MONTH FROM date) = $2
		AND person = $3
		`, parsedDate.Year(), int(parsedDate.Month()), stats.Person.ID)

	if err != nil {
		return Record{}, err
	}

	if !exists {
		return create(stats)
	}

	return update(stats)
}

// GetForDateRangeByPerson returns all Statistic that fall within the given date range, grouped by person.
func GetForDateRangeByPerson(start string, end string) (map[string][]Record, error) {
	// var list []Record
	list := make(map[string][]Record)
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return map[string][]Record{}, err
	}

	defer db.Close()

	rows, err := db.Query(`
		SELECT * FROM statusboard.stats s
		JOIN statusboard.people p ON s.person = p.id
		WHERE s.date BETWEEN $1 AND $2
		AND p.type NOT IN (2,3,4)
		ORDER BY s.date ASC;
	`, start, end)

	if err != nil {
		return map[string][]Record{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Record
		var scannedDate time.Time
		var handle string

		err = rows.Scan(
			&scanned.ID,
			&scanned.Person.ID,
			&scannedDate,
			&scanned.Stats.CustomerSatisfaction,
			&scanned.Stats.SameDay,
			&scanned.Stats.AverageTickets,
			&scanned.Stats.TicketTime,
			&scanned.Person.ID,
			&scanned.Person.Name,
			&handle,
			&scanned.Person.Title,
			&scanned.Person.Active,
			&scanned.Person.Type,
			&scanned.Person.GeekOfTheWeek,
			&scanned.Person.GitlabID,
			&scanned.Person.GeekOfTheMonth,
		)

		scanned.Person.Handle = strings.ToLower(handle)

		if err != nil {
			return map[string][]Record{}, err
		}

		scanned.Date = scannedDate.Format("2006-01-02")

		list[scanned.Person.Handle] = append(list[scanned.Person.Handle], scanned)

		// list = append(list, scanned)
	}

	return list, rows.Err()
}
