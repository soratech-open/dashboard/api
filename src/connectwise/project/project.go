package project

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise"
)

type projectCount struct {
	Count int `json:"count"`
}

// CountInRange returns the number of projects with estimated start dates in the given range.
func CountInRange(start string, end string) (int, error) {
	var projects projectCount
	query := url.Values{}
	urlString := fmt.Sprintf("%s/project/projects/count", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return 0, err
	}

	query.Add("conditions", fmt.Sprintf("estimatedStart>=[%s] AND estimatedStart<=[%s]", start, end))
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return 0, err
	}

	err = json.Unmarshal(results, &projects)

	if err != nil {
		return 0, err
	}

	return projects.Count, nil
}

// CountForCurrentYear returns the number of projects started in the current year.
func CountForCurrentYear() (int, error) {
	var count projectCount
	currentYear := time.Now().Year()
	query := url.Values{}
	urlString := fmt.Sprintf("%s/project/projects/count", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return 0, err
	}

	query.Add("conditions", fmt.Sprintf("estimatedStart>=[%d-01-01] AND estimatedStart<=[%d-12-31]", currentYear, currentYear))
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return 0, err
	}

	err = json.Unmarshal(results, &count)

	if err != nil {
		return 0, err
	}

	return count.Count, nil

}
