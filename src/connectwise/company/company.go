package company

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise"
)

// Company represents a single company.
type Company struct {
	ID         int                 `json:"id"`
	Identifier string              `json:"identifier"`
	Name       string              `json:"name"`
	Status     connectwise.Contact `json:"status"`
	Address1   string              `json:"addressLine1"`
	Address2   string              `json:"addressLine2"`
	City       string              `json:"city"`
	State      string              `json:"state"`
	Zip        string              `json:"zip"`
	Phone      string              `json:"phoneNumber"`
	Fax        string              `json:"faxNumber"`
	Website    string              `json:"website"`
	Acquired   time.Time           `json:"dateAcquired"`
}

func getCompanies(conditions string) ([]Company, error) {
	query := url.Values{}
	var list []Company

	urlString := fmt.Sprintf("%s/company/companies", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return []Company{}, err
	}

	query.Add("conditions", conditions)
	query.Add("pageSize", "1000")
	query.Add("orderBy", "name")
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return []Company{}, err
	}

	err = json.Unmarshal(results, &list)

	return list, err
}

// GetByStatus returns all companies with the given status.
func GetByStatus(status int) ([]Company, error) {
	conditions := fmt.Sprintf("status/id=%d", status)
	return getCompanies(conditions)
}

// GetByID returns the company with the given id.
func GetByID(id int) (Company, error) {
	var company Company
	urlString := fmt.Sprintf("%s/company/companies/%d", connectwise.GetBaseURL(), id)

	results, err := connectwise.CallAPI(urlString)

	if err != nil {
		return Company{}, err
	}

	err = json.Unmarshal(results, &company)

	return company, err
}

// GetByIdentifier returns any company with the given identifier
func GetByIdentifier(identifier string) ([]Company, error) {
	conditions := fmt.Sprintf("identifier=\"%s\"", identifier)
	return getCompanies(conditions)
}

// GetHourlyIDs returns the ID's of all hourly clients as a comma separated string.
func GetHourlyIDs() (string, error) {
	var companyIds []string
	companies, err := GetByStatus(19)

	if err != nil {
		return "", err
	}

	for _, company := range companies {
		idString := strconv.Itoa(company.ID)

		companyIds = append(companyIds, idString)
	}

	idString := strings.Join(companyIds, ", ")

	return idString, nil
}
