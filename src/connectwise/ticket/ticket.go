package ticket

import (
	"encoding/json"
	"fmt"
	"math"
	"net/mail"
	"net/url"
	"strings"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/email"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/company"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/timeentry"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise"
)

// Ticket represents a single ticket.
type Ticket struct {
	ID           int                 `json:"id"`
	Summary      string              `json:"summary"`
	Board        details             `json:"board"`
	Status       details             `json:"status"`
	Company      connectwise.Company `json:"company"`
	Contact      details             `json:"contact"`
	Type         details             `json:"type"`
	SubType      details             `json:"subType"`
	Item         details             `json:"item"`
	Priority     priority            `json:"priority"`
	ClosedDate   time.Time           `json:"closedDate"`
	Closed       bool                `json:"closedFlag"`
	Hours        float64             `json:"actualHours"`
	DateResolved time.Time           `json:"dateResolved"`
	Resources    string              `json:"resources"`
	Info         info                `json:"_info"`
	Owner        connectwise.Company `json:"owner"`
}

type details struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type priority struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Sort int    `json:"sort"`
}

type info struct {
	LastUpdated time.Time `json:"lastUpdated"`
	UpdatedBy   string    `json:"updatedBy"`
	DateEntered time.Time `json:"dateEntered"`
}

type countResult struct {
	Count int `json:"count"`
}

func getTickets(conditions string) ([]Ticket, error) {
	query := url.Values{}
	var list []Ticket

	urlString := fmt.Sprintf("%s/service/tickets", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return []Ticket{}, err
	}

	query.Add("conditions", conditions)
	query.Add("pageSize", "1000")
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return []Ticket{}, err
	}

	err = json.Unmarshal(results, &list)

	return list, err
}

// GetForBoardInRange returns all tickets on the given board entered in the given range.
func GetForBoardInRange(board int, start string, end string) ([]Ticket, error) {
	conditions := fmt.Sprintf("board/id=%d AND dateEntered>=[%s] AND dateEntered<[%s]", board, start, end)
	return getTickets(conditions)

}

// CompletedForBoardInRange returns all tickets on the given board that were completed in the given range.
func CompletedForBoardInRange(board int, start string, end string) ([]Ticket, error) {
	conditions := fmt.Sprintf("board/id=%d AND dateResolved>=[%s] AND dateResolved<[%s] AND status/name=\"Completed\"", board, start, end)
	return getTickets(conditions)
}

// ClosedForBoardInRange returns all closed tickets on the given board that were closed in the given range.
func ClosedForBoardInRange(board int, start string, end string) ([]Ticket, error) {
	conditions := fmt.Sprintf("board/id=%d AND closedDate>=[%s] AND closedDate<[%s] AND closedFlag=true", board, start, end)
	return getTickets(conditions)
}

// OpenedCountForBoardInRange returns the count of all tickets opened on the given board in the given range.
func OpenedCountForBoardInRange(board int, start string, end string) (int, error) {
	conditions := fmt.Sprintf("board/id=%d AND dateEntered>=[%s] AND dateEntered<=[%s] AND parentTicketId=null AND status/id!=237", board, start, end)
	return getTicketCount(conditions)
}

// GetOne returns the ticket with the given id.
func GetOne(id int) (Ticket, error) {
	var ticket Ticket
	url := fmt.Sprintf("%s/service/tickets/%d", connectwise.GetBaseURL(), id)

	result, err := connectwise.CallAPI(url)

	if err != nil {
		return Ticket{}, err
	}

	err = json.Unmarshal(result, &ticket)

	return ticket, err
}

// GetPrioritiesForBoard returns all the priorities for the given board.
func GetPrioritiesForBoard(board int) ([]Ticket, error) {
	var statuses string
	conditions := strings.Builder{}
	userSupportStatuses := []string{"15", "17", "31", "34", "118", "237", "391", "444", "509"}
	netAdminStatuses := []string{"142", "144", "145", "146", "151", "446", "508"}

	switch board {
	case 1:
		statuses = strings.Join(userSupportStatuses, ", ")
	case 13:
		statuses = strings.Join(netAdminStatuses, ", ")
	}

	conditions.WriteString(fmt.Sprintf("board/id=%d ", board))
	conditions.WriteString(fmt.Sprintf("AND status/id NOT IN (%s) ", statuses))
	conditions.WriteString("AND priority/id NOT IN (3) ")
	conditions.WriteString("AND resources!= null")
	return getTickets(conditions.String())
}

// ResolvedForBoardInRange returns all resolved tickets on the given board in the given range.
func ResolvedForBoardInRange(board int, start string, end string) ([]Ticket, error) {
	var statuses string
	userStatuses := []string{"17", "31", "118", "391", "549"}
	netStatuses := []string{"144", "145", "151"}

	switch board {
	case 1:
		statuses = strings.Join(userStatuses, ", ")
	case 13:
		statuses = strings.Join(netStatuses, ", ")
	}

	conditions := fmt.Sprintf("board/id=%d AND dateEntered>=[%s] AND dateEntered<[%s] AND status/id IN (%s)", board, start, end, statuses)

	return getTickets(conditions)
}

//GetSameDayCloseInRange calculates each tech's same day close percentage ard returns it in a struct.
func GetSameDayCloseInRange(start string, end string) ([]connectwise.Stat, error) {
	var list []connectwise.Stat
	totalMap := make(map[string]float64)
	counterMap := make(map[string]float64)

	closed, err := ClosedForBoardInRange(1, start, end)

	if err != nil {
		return list, err
	}

	for _, resolved := range closed {
		timeOpen := resolved.DateResolved.Sub(resolved.Info.DateEntered)

		if float64(timeOpen.Hours()) <= 24 {
			totalMap[resolved.Owner.Identifier]++
		}

		counterMap[resolved.Owner.Identifier]++
	}

	for name, total := range totalMap {
		average := (total / counterMap[name]) * 100

		stat := connectwise.Stat{
			Type:   "Same Day",
			Handle: name,
			Score:  average,
		}

		list = append(list, stat)
	}

	return list, nil
}

// GetAverageClosedInRange returns the average number of tickets closed per day in the given range for each tech.
func GetAverageClosedInRange(start string, end string) ([]connectwise.Stat, error) {
	var list []connectwise.Stat

	daysWorked, err := timeentry.GetWorkedCountInRange(start, end)

	if err != nil {
		return []connectwise.Stat{}, err
	}

	counts, err := GetCountForBoardInRange(1, start, end)

	if err != nil {
		return []connectwise.Stat{}, err
	}

	for _, worked := range daysWorked {
		if worked.Count > 0 {
			count := float64(counts[worked.Name])
			average := count / float64(worked.Count)

			stat := connectwise.Stat{
				Type:   "Average Tickets",
				Handle: worked.Name,
				Score:  math.Round(average*100) / 100,
			}

			list = append(list, stat)
		}

	}

	return list, nil
}

// GetTimeInRange returns each technician's average time spent per ticket in the given range.
func GetTimeInRange(start string, end string) ([]connectwise.Stat, error) {
	var list []connectwise.Stat
	timeMap := make(map[string]float64)
	countMap := make(map[string]float64)

	tickets, err := ClosedForBoardInRange(1, start, end)

	if err != nil {
		return list, err
	}

	for _, ticket := range tickets {
		timeMap[ticket.Owner.Identifier] += ticket.Hours
		countMap[ticket.Owner.Identifier]++
	}

	for tech, hours := range timeMap {
		average := (hours / countMap[tech]) * 60

		stat := connectwise.Stat{
			Type:   "Ticket Time",
			Handle: tech,
			Score:  math.Round(average*100) / 100,
		}

		list = append(list, stat)
	}

	return list, nil
}

// GetCountForBoardInRange returns the number of tickets each tech completed in the given range.
func GetCountForBoardInRange(board int, start string, end string) (map[string]int, error) {
	tickets, err := ClosedForBoardInRange(1, start, end)
	counter := make(map[string]int)

	if err != nil {
		return map[string]int{}, err
	}

	for _, ticket := range tickets {
		counter[ticket.Owner.Identifier]++
	}

	return counter, nil
}

// ClosedCountForBoardAndPersonInRange returns all closed tickets on the given board that were closed in the given range.
func ClosedCountForBoardAndPersonInRange(board int, start string, end string, handle string) (int, error) {
	conditions := fmt.Sprintf("board/id=%d AND closedDate>=[%s] AND closedDate<[%s] AND closedFlag=true AND owner/identifier=\"%s\"", board, start, end, handle)
	return getTicketCount(conditions)
}

// ClosedCountForBoardInRange returns all closed tickets on the given board that were closed in the given range.
func ClosedCountForBoardInRange(board int, start string, end string) (int, error) {
	conditions := fmt.Sprintf("board/id=%d AND dateEntered>=[%s] AND dateEntered<[%s] AND closedFlag=true", board, start, end)
	return getTicketCount(conditions)
}

func getTicketCount(conditions string) (int, error) {
	query := url.Values{}

	var counted countResult

	urlString := fmt.Sprintf("%s/service/tickets/count", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return 0, err
	}

	query.Add("conditions", conditions)
	query.Add("pageSize", "1000")
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return 0, err
	}

	err = json.Unmarshal(results, &counted)

	return counted.Count, err
}

// Count represents the count of tickets a tech has closed on a single board.
type Count struct {
	Handle string
	Board  int
	Count  int
}

// GetTotalClosedForPersonInRange returns the number of tickets closed on each board by the given person in the given range.
func GetTotalClosedForPersonInRange(handle string, start string, end string) ([]Count, error) {
	var list []Count
	boards := []int{1, 11, 13}
	countChan := make(chan Count, len(boards))

	for _, board := range boards {
		go func(b int, h string, s string, e string) {
			count, err := ClosedCountForBoardAndPersonInRange(b, s, e, h)

			if err != nil {
				panic(err)
			}

			total := Count{
				Handle: h,
				Board:  b,
				Count:  count,
			}

			countChan <- total
		}(board, handle, start, end)
	}

	for range boards {
		counted := <-countChan
		list = append(list, counted)
	}

	return list, nil
}

// GetPriorities returns the current day's priorities.
func GetPriorities() (map[string][]Ticket, error) {
	list := make(map[string][]Ticket)
	priorityChannel := make(chan []Ticket)
	boards := []int{1, 13}

	for _, id := range boards {
		go func(board int) {
			tickets, err := GetPrioritiesForBoard(board)

			if err != nil {
				panic(err)
			}

			priorityChannel <- tickets
		}(id)

		results := <-priorityChannel

		for _, result := range results {
			resources := strings.Split(result.Resources, ", ")

			for _, resource := range resources {
				list[resource] = append(list[resource], result)
			}
		}

	}

	return list, nil
}

type total struct {
	Opened int `json:"opened"`
	Closed int `json:"closed"`
}

// TotalTicketsInRange returns the total number of tickets opened and closed in the given range.
func TotalTicketsInRange(start string, end string) map[string]total {
	list := make(map[string]total)
	boards := []int{1, 13}
	openedChannel := make(chan int)
	closedChannel := make(chan int)
	var openTotal int
	var closedTotal int

	for _, id := range boards {
		go func(b int, s string, e string) {
			count, err := OpenedCountForBoardInRange(b, s, e)

			if err != nil {
				panic(err)
			}

			openedChannel <- count
		}(id, start, end)

		go func(b int, s string, e string) {
			count, err := ClosedCountForBoardInRange(b, s, e)

			if err != nil {
				panic(err)
			}

			closedChannel <- count
		}(id, start, end)

		opened := <-openedChannel
		closed := <-closedChannel

		totl := total{
			Opened: opened,
			Closed: closed,
		}

		openTotal += opened
		closedTotal += closed

		list[connectwise.GetBoardName(id)] = totl
	}

	list["Total"] = total{
		Opened: openTotal,
		Closed: closedTotal,
	}

	return list
}

// GetOpenHourly returns all open tickets for hourly clients
func GetOpenHourly() ([]Ticket, error) {
	idString, err := company.GetHourlyIDs()

	if err != nil {
		return []Ticket{}, err
	}

	conditions := fmt.Sprintf("company/id IN (%s) AND closedFlag=false AND board/id IN (1, 11, 13)", idString)

	return getTickets(conditions)
}

// GetClosedHourlyInRange returns a list of all hourly tickets closed in the given range.
func GetClosedHourlyInRange(start string, end string) ([]Ticket, error) {
	idString, err := company.GetHourlyIDs()

	if err != nil {
		return []Ticket{}, err
	}

	conditions := fmt.Sprintf("company/id IN (%s) AND closedFlag=true AND closedDate>=[%s] AND closedDate<=[%s] AND board/id IN (1, 11, 13)", idString, start, end)

	return getTickets(conditions)
}

//GetCompanySameDayCloseInRange calculates the company's same day close percentage ard returns it.
func GetCompanySameDayCloseInRange(start string, end string) (float64, error) {
	var total float64
	var counter float64

	closed, err := ClosedForBoardInRange(1, start, end)

	if err != nil {
		return 0, err
	}

	for _, resolved := range closed {
		timeOpen := resolved.DateResolved.Sub(resolved.Info.DateEntered)

		if float64(timeOpen.Hours()) <= 24 {
			total++
		}

		counter++
	}

	return (total / counter) * 100, nil
}

// GetCompanyTicketTimeInRange returns the average time spent in tickets for the whole company.
func GetCompanyTicketTimeInRange(start string, end string) (float64, error) {
	var hours float64
	var count float64

	tickets, err := ClosedForBoardInRange(1, start, end)

	if err != nil {
		return 0, err
	}

	for _, ticket := range tickets {
		count++
		hours += ticket.Hours
	}

	return hours / count * 100, nil
}

// GetCompanyAverageClosedInRange returns the company's average tickets closed in the given range.
func GetCompanyAverageClosedInRange(start string, end string) (float64, error) {
	results, err := GetAverageClosedInRange(start, end)
	var total float64

	if err != nil {
		return 0, err
	}

	for _, result := range results {
		total += result.Score
	}

	average := total / float64(len(results))

	return average, nil
}

// GetTicketsClosedForMonth returns the number of tickets each tech closed in the given month.
func GetTicketsClosedForMonth(date string) (map[string]int, error) {
	boards := []int{1, 11, 13}
	list := make(map[string]int)
	var tickets []Ticket
	resultChannel := make(chan []Ticket)
	given, err := time.Parse("2006-01", date)

	if err != nil {
		return list, err
	}
	month := given.AddDate(0, 1, 0)
	start := fmt.Sprintf("%s-01", date)
	end := month.Format("2006-01-02")

	for _, board := range boards {
		go func(b int, s string, e string) {
			results, err := ClosedForBoardInRange(b, s, e)

			if err != nil {
				panic(err)
			}

			resultChannel <- results
		}(board, start, end)

		result := <-resultChannel

		for _, ticket := range result {
			tickets = append(tickets, ticket)
		}
	}

	for _, item := range tickets {
		if item.Owner.Name != "" {
			list[strings.ToLower(item.Owner.Identifier)]++
		}
	}

	return list, nil
}

// GetAllOpen returns all currently open tickets.
func getAllOpen() ([]Ticket, error) {
	conditions := "board/id IN (1,11,13) AND closedFlag=false AND parentTicketId=null AND resources!=null"
	return getTickets(conditions)
}

func sortTicketsByResource(tickets []Ticket) map[string][]Ticket {
	list := make(map[string][]Ticket)

	for _, ticket := range tickets {
		resources := strings.Split(ticket.Resources, ", ")

		for _, resource := range resources {
			switch {
			case resource == "JTrout1":
				list["jtrout"] = append(list[resource], ticket)
			case len(resource) >= 1:
				list[resource] = append(list[resource], ticket)
			}
		}
	}

	return list
}

// OpenTicketsEmail sends emails to all the techs containing a list of all their open tickets.
func OpenTicketsEmail() error {
	tickets, err := getAllOpen()

	if err != nil {
		return err
	}

	sorted := sortTicketsByResource(tickets)
	errorChannel := make(chan error)

	for name, open := range sorted {

		go func(n string, t []Ticket) {
			table := makeTicketTable(t)

			message := email.Email{
				From:    mail.Address{Name: "Ticket Terminator", Address: "alerts@soratech.com"},
				To:      mail.Address{Address: fmt.Sprintf("%s@soratech.com", n)},
				Subject: "Current Open Tickets",
				Body:    table,
			}

			errorChannel <- email.SendHTML(message)
		}(name, open)

		result := <-errorChannel

		if result != nil {
			return result
		}
	}

	return nil
}

// makeTicketTable makes an HTML table from an array of surveys. Useful for email reports.
func makeTicketTable(tickets []Ticket) string {
	var table strings.Builder

	table.WriteString(`
	<html>
	<body>
	<style>
		table {
			width:100%;
			border:1px solid #000;
			border-collapse:collapse;
		}

		th, td {
			border:1px solid #000;
		}
	</style>
	<table>
		<thead>
			<tr>
				<th>Ticket Number</th>
				<th>Company</th>
				<th>Summary</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
	`)

	for _, ticket := range tickets {
		row := fmt.Sprintf(`
			<tr>
				<td>%d</td>
				<td>%s</td>
				<td>%s</td>
				<td>%s</td>
			</tr>
		`, ticket.ID, ticket.Company.Identifier, ticket.Summary, ticket.Status.Name)

		table.WriteString(row)
	}

	table.WriteString("</tbody></table></body></html>")

	return table.String()
}
