package survey

import (
	"encoding/json"
	"fmt"
	"math"
	"net/mail"
	"net/url"
	"strings"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/email"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/ticket"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise"
)

// Survey represents a single survey result.
type Survey struct {
	ID          int                 `json:"id"`
	TicketID    int                 `json:"ticketId"`
	TotalPoints int                 `json:"totalPoints"`
	Company     connectwise.Company `json:"company"`
	Owner       connectwise.Company `json:"owner"`
	Contact     string              `json:"contact"`
	Comments    string              `json:"footerResponse"`
}

// GetInRange gets all surveys answered in the given date range.
func GetInRange(start string, end string) ([]Survey, error) {
	query := url.Values{}
	var list []Survey

	urlString := fmt.Sprintf("%s/service/surveys/3/results", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return []Survey{}, err
	}

	query.Add("conditions", fmt.Sprintf("dateEntered>=[%s] AND dateEntered<[%s]", start, end))
	query.Add("pageSize", "1000")
	query.Add("orderBy", "totalPoints")
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return []Survey{}, err
	}

	err = json.Unmarshal(results, &list)

	for index, item := range list {
		item, err = getAssociatedTicket(item)

		if err != nil {
			return []Survey{}, err
		}

		list[index] = item
	}

	return list, nil
}

// GetCustomerSatisfaction returns the customer satisfaction percentage for all answered surveys in the given date range.
func GetCustomerSatisfaction(start string, end string) ([]connectwise.Stat, error) {
	var satisfaction []connectwise.Stat
	rawScoreMap := make(map[string]float64)
	counterMap := make(map[string]int)

	surveys, err := GetInRange(start, end)

	if err != nil {
		return []connectwise.Stat{}, err
	}

	for _, survey := range surveys {
		score := float64(survey.TotalPoints)

		rawScoreMap[survey.Owner.Identifier] += score
		counterMap[survey.Owner.Identifier]++
	}

	for name, score := range rawScoreMap {
		average := (score / float64(counterMap[name]) * 2) * 10
		total := connectwise.Stat{
			Handle: name,
			Score:  math.Round(average*100) / 100, //to round the average to two decimal places
			Type:   "Customer Satisfaction",
		}

		satisfaction = append(satisfaction, total)
	}

	return satisfaction, nil
}

func getAssociatedTicket(response Survey) (Survey, error) {
	associated, err := ticket.GetOne(response.TicketID)

	if err != nil {
		return Survey{}, err
	}

	response.Owner = associated.Owner
	response.Contact = associated.Contact.Name

	return response, nil
}

// GetForPersonInRange returns a list of all survey responses for the given person in the given range.
func GetForPersonInRange(handle string, start string, end string) ([]Survey, error) {
	var list []Survey
	results, err := GetInRange(start, end)

	if err != nil {
		return []Survey{}, err
	}

	surveyChannel := make(chan Survey)
	nilChannel := make(chan int)

	for _, result := range results {
		go func(s Survey) {
			response, err := getAssociatedTicket(s)

			if err != nil {
				panic(err)
			}

			if response.Owner.Identifier == handle {
				surveyChannel <- response
			} else {
				nilChannel <- 0
			}
		}(result)

		select {
		case matched := <-surveyChannel:
			list = append(list, matched)
		case <-nilChannel:
			// do nothing
		}
	}

	return list, nil
}

// GetCustomerSatisfactionForCompany returns the Company's customer satisfaction rating in the given range.
func GetCustomerSatisfactionForCompany(start string, end string) (float64, error) {
	var counter float64
	var score float64

	results, err := GetInRange(start, end)

	if err != nil {
		return 0, err
	}

	for _, survey := range results {
		counter++
		score += float64(survey.TotalPoints)
	}

	score = score * 2

	average := (score / counter) * 10

	return math.Round(average*100) / 100, nil
}

func getBad() ([]Survey, error) {
	now := time.Now()
	today := now.Format("2006-01-02")
	yesterday := now.AddDate(0, 0, -1).Format("2006-01-02")
	query := url.Values{}
	var list []Survey

	urlString := fmt.Sprintf("%s/service/surveys/3/results", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return []Survey{}, err
	}

	query.Add("conditions", fmt.Sprintf("dateEntered>=[%s] AND dateEntered<[%s] AND totalPoints<4", yesterday, today))
	query.Add("pageSize", "1000")
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return []Survey{}, err
	}

	err = json.Unmarshal(results, &list)

	for index, item := range list {
		item, err = getAssociatedTicket(item)

		if err != nil {
			return []Survey{}, err
		}

		list[index] = item
	}

	return list, nil
}

// makeTable makes an HTML table from an array of surveys. Useful for email reports.
func makeTable(surveys []Survey) string {
	var table strings.Builder

	table.WriteString(`
	<html>
	<body>
	<style>
		table {
			width:100%;
			border:1px solid #000;
			border-collapse:collapse;
		}

		th, td {
			border:1px solid #000;
		}
	</style>
	<p>The following surveys with scores of 3 or lower were received yesterday</p>
	<table>
		<thead>
			<tr>
				<th>Ticket Number</th>
				<th>Technician</th>
				<th>Company</th>
				<th>Contact</th>
				<th>Score</th>
				<th>Comments</th>
			</tr>
		</thead>
		<tbody>
	`)

	for _, survey := range surveys {
		row := fmt.Sprintf(`
			<tr>
				<td>%d</td>
				<td>%s</td>
				<td>%s</td>
				<td>%s</td>
				<td>%d</td>
				<td>%s</td>
			</tr>
		`, survey.TicketID, survey.Owner.Name, survey.Company.Identifier, survey.Contact, survey.TotalPoints, survey.Comments)

		table.WriteString(row)
	}

	table.WriteString("</tbody></table></body></html>")

	return table.String()
}

// BadEmail sends an email containing any surveys with scores of 3 or below that were answered yesterday.
func BadEmail() error {
	body := "No surveys to report"
	now := time.Now()
	yesterday := now.AddDate(0, 0, -1).Format("01/02/2006")
	surveys, err := getBad()

	if err != nil {
		return err
	}

	if len(surveys) > 0 {
		body = makeTable(surveys)
	}

	message := email.Email{
		From:    mail.Address{Name: "Ticket Terminator", Address: "alerts@soratech.com"},
		To:      mail.Address{Address: "surveyresults@soratech.com"},
		Subject: fmt.Sprintf("Low scoring survey report %s", yesterday),
		Body:    body,
	}

	return email.SendHTML(message)
}
