package connectwise

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/secret"
)

// Company represents a company reference from the ConnectWiseAPI.
type Company struct {
	ID         int    `json:"id"`
	Identifier string `json:"identifier"`
	Name       string `json:"name"`
}

// Stat is a generic struct for holding a tech's stats.
type Stat struct {
	Type   string  `json:"type"`
	Handle string  `json:"handle"`
	Score  float64 `json:"score"`
}

// Contact is a generic struct for holding a ticket's contact (or anything else with matching fields).
type Contact struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// GetBaseURL returns the base URL for the ConnectWise API.
func GetBaseURL() string {
	return "https://support.soratech.com/v4_6_release/apis/3.0"
}

// CallAPI makes a GET request to the given URL.
func CallAPI(url string) ([]byte, error) {
	client := &http.Client{
		Timeout: time.Second * 30,
	}

	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return []byte{}, err
	}

	secrets, err := secret.Get()

	if err != nil {
		return []byte{}, err
	}

	authToken := fmt.Sprintf("Basic %s", secrets.Connectwise.Token)

	request.Header.Add("Authorization", authToken)
	request.Header.Add("clientId", secrets.Connectwise.ClientID)

	response, err := client.Do(request)

	if err != nil {
		return []byte{}, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	return body, err
}

// GetBoardName returns the name of the board with the given id.
func GetBoardName(id int) string {
	var name string
	switch id {
	case 1:
		name = "UserSupport"
	case 11:
		name = "Proactive"
	case 13:
		name = "NetAdmin"
	}

	return name
}
