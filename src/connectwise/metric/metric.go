package metric

import (
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/survey"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/ticket"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/stats"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/stats/current"
)

// TechStat is a collection of Stats tied to a specific tech.
type TechStat struct {
	Handle string      `json:"handle"`
	Stats  stats.Stats `json:"stats"`
	Start  string      `json:"start"`
	End    string      `json:"end"`
}

// GetStatsInRange returns all the relevant stats from the given range.
func GetStatsInRange(start string, end string) ([]TechStat, error) {
	var list []TechStat
	satisfaction, err := survey.GetCustomerSatisfaction(start, end)

	if err != nil {
		return []TechStat{}, err
	}

	sameDay, err := ticket.GetSameDayCloseInRange(start, end)

	if err != nil {
		return []TechStat{}, err
	}

	averageTickets, err := ticket.GetAverageClosedInRange(start, end)

	if err != nil {
		return []TechStat{}, err
	}

	ticketTime, err := ticket.GetTimeInRange(start, end)

	if err != nil {
		return []TechStat{}, err
	}

	for _, cs := range satisfaction {
		var stat TechStat
		endTime, _ := time.Parse("2006-01-02", end)
		lastDay := endTime.AddDate(0, 0, -1).Format("2006-01-02")

		stat.Handle = cs.Handle
		stat.Stats.CustomerSatisfaction = cs.Score
		stat.Start = start
		stat.End = lastDay

		for _, sd := range sameDay {
			if sd.Handle == stat.Handle {
				stat.Stats.SameDay = sd.Score
			}
		}

		for _, at := range averageTickets {
			if at.Handle == stat.Handle {
				stat.Stats.AverageTickets = at.Score
			}
		}

		for _, tt := range ticketTime {
			if tt.Handle == stat.Handle {
				stat.Stats.TicketTime = tt.Score
			}
		}

		list = append(list, stat)
	}

	companyStats, err := GetCompanyStatsInRange(start, end)

	if err != nil {
		return []TechStat{}, err
	}

	list = append(list, companyStats)

	return list, nil
}

// MakeMonthStats converts a slice of TechStat into a slice of stats.Record
func MakeMonthStats(data []TechStat) ([]stats.Record, error) {
	var list []stats.Record

	for _, stat := range data {
		var record stats.Record

		tech, err := person.GetByHandle(stat.Handle)

		if err != nil {
			continue
		}

		record.Person = tech
		record.Stats = stat.Stats
		record.Date = stat.End

		list = append(list, record)
	}

	return list, nil
}

// MakeCurrentStats converts a slice of TechStat into a slice of current.Record
func MakeCurrentStats(data []TechStat) ([]current.Record, error) {
	var list []current.Record

	for _, stat := range data {
		var record current.Record

		tech, err := person.GetByHandle(stat.Handle)

		if err != nil {
			continue
		}

		record.Person = tech
		record.Stats = stat.Stats
		record.LastUpdated = time.Now()

		list = append(list, record)
	}

	return list, nil
}

// GetCompanyStatsInRange returns all the relevant stats from the given range.
func GetCompanyStatsInRange(start string, end string) (TechStat, error) {
	satisfactionChannel := make(chan float64)
	sameDayChannel := make(chan float64)
	averageChannel := make(chan float64)
	ticketTimeChannel := make(chan float64)
	endTime, _ := time.Parse("2006-01-02", end)
	lastDay := endTime.AddDate(0, 0, -1).Format("2006-01-02")

	go func(s string, e string) {
		satisfaction, err := survey.GetCustomerSatisfactionForCompany(start, end)

		if err != nil {
			panic(err)
		}

		satisfactionChannel <- satisfaction
	}(start, end)

	go func(s string, e string) {
		sameDay, err := ticket.GetCompanySameDayCloseInRange(s, e)

		if err != nil {
			panic(err)
		}

		sameDayChannel <- sameDay
	}(start, end)

	go func(s string, e string) {
		averageTickets, err := ticket.GetCompanyAverageClosedInRange(s, e)

		if err != nil {
			panic(err)
		}

		averageChannel <- averageTickets
	}(start, end)

	go func(s string, e string) {
		ticketTime, err := ticket.GetCompanyTicketTimeInRange(start, end)

		if err != nil {
			panic(err)
		}

		ticketTimeChannel <- ticketTime
	}(start, end)

	sat := <-satisfactionChannel
	sd := <-sameDayChannel
	av := <-averageChannel
	tt := <-ticketTimeChannel

	var stat TechStat

	stat.Handle = "Sora"
	stat.Start = start
	stat.End = lastDay
	stat.Stats.CustomerSatisfaction = sat
	stat.Stats.SameDay = sd
	stat.Stats.AverageTickets = av
	stat.Stats.TicketTime = tt

	return stat, nil
}
