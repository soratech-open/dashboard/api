package timeentry

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise"
)

// Entry represents a single time entry from the ConnectWise API
type Entry struct {
	ID         int                 `json:"id"`
	ChargeCode int                 `json:"chargeToId"`
	Person     connectwise.Company `json:"member"`
	Start      time.Time           `json:"timeStart"`
	End        time.Time           `json:"timeEnd"`
	Hours      float64             `json:"actualHours"`
}

func getEntries(conditions string) ([]Entry, error) {
	query := url.Values{}
	var list []Entry

	urlString := fmt.Sprintf("%s/time/entries", connectwise.GetBaseURL())

	baseURL, err := url.Parse(urlString)

	if err != nil {
		return []Entry{}, err
	}

	query.Add("conditions", conditions)
	query.Add("pageSize", "1000")
	query.Add("orderBy", "timeStart ASC")
	baseURL.RawQuery = query.Encode()

	results, err := connectwise.CallAPI(baseURL.String())

	if err != nil {
		return []Entry{}, err
	}

	err = json.Unmarshal(results, &list)

	return list, err
}

// GetForRange returns all time entries in the given time range.
func GetForRange(start string, end string) ([]Entry, error) {
	conditions := fmt.Sprintf("timeStart>=[%s] AND timeStart<=[%s]", start, end)
	return getEntries(conditions)
}

// GetWorkedInRange returns all time entries in the given time range that are not time off.
func GetWorkedInRange(start string, end string) ([]Entry, error) {
	conditions := fmt.Sprintf("timeStart>=[%s] AND timeStart<[%s] AND chargeToId NOT IN (2,3,4,32,43,50,52)", start, end)
	return getEntries(conditions)
}

// GetForPersonInRange return all time entries in the given range for the given person
func GetForPersonInRange(handle string, start string, end string) ([]Entry, error) {
	conditions := fmt.Sprintf("timeStart>=[%s] AND timeStart<=[%s] AND member/identifier=\"%s\"", start, end, handle)
	return getEntries(conditions)
}

// GetWorkedForPersonInRange returns all time entries for the given person in the given time range that are not time off.
func GetWorkedForPersonInRange(handle string, start string, end string) ([]Entry, error) {
	conditions := fmt.Sprintf("timeStart>=[%s] AND timeStart<[%s] AND chargeToId NOT IN (2,3,4,32,43,50,52) AND member/identifier=\"%s\"", start, end, handle)
	return getEntries(conditions)
}

// GetOffForPersonInRange returns all time off taken in the given range by the given person.
func GetOffForPersonInRange(handle string, start string, end string) ([]Entry, error) {
	conditions := fmt.Sprintf("timeStart>=[%s] AND timeStart<=[%s] AND chargeToId IN (2,3,4,32,43,50,52) AND member/identifier=\"%s\"", start, end, handle)
	return getEntries(conditions)
}

// WorkedCount contains a tech's name and the number of days worked.
type WorkedCount struct {
	Name  string
	Count int
}

func getWorkedCountForPersonInRange(handle string, start string, end string) (WorkedCount, error) {
	var dates []string
	entries, err := GetWorkedForPersonInRange(handle, start, end)

	if err != nil {
		return WorkedCount{}, err
	}

	for _, entry := range entries {
		formattedStart := entry.Start.Format("2006-01-02")

		if !sliceContains(dates, formattedStart) {
			dates = append(dates, formattedStart)
		}
	}

	count := WorkedCount{
		Name:  handle,
		Count: len(dates),
	}

	return count, err
}

// GetWorkedCountInRange returns the number of days each tech worked in the given time period.
func GetWorkedCountInRange(start string, end string) ([]WorkedCount, error) {
	var counts []WorkedCount

	techs, err := person.GetActive(true)

	if err != nil {
		return []WorkedCount{}, err
	}

	countChannel := make(chan WorkedCount, len(techs))

	for _, tech := range techs {
		if tech.Type == 1 || tech.Type == 2 {
			go func(p person.Person) {
				count, err := getWorkedCountForPersonInRange(p.Handle, start, end)

				if err != nil {
					panic(err)
				}

				countChannel <- count

			}(tech)

			counted := <-countChannel
			counts = append(counts, counted)
		}
	}

	return counts, nil
}

func sliceContains(haystack []string, needle string) bool {
	for _, a := range haystack {
		if a == needle {
			return true
		}
	}

	return false
}

/*
	2 : Vacation
	3 : Personal Time
	4 : Holiday,
	32 : Sick
	43 : Jury Duty
	50 : Unpaid Vacation
	52 : Bereavement
*/

// TimeOff represents the amount of time off taken by a single person.
type TimeOff struct {
	Name         string  `json:"name"`
	Vacation     float64 `json:"vacation"`
	PersonalTime float64 `json:"personalTime"`
	Holiday      float64 `json:"holiday"`
	Sick         float64 `json:"sick"`
	Jury         float64 `json:"jury"`
	Unpaid       float64 `json:"unpaid"`
	Bereavement  float64 `json:"bereavement"`
}

// GetTimeOffUsedForPerson returns the amount of time off taken by the given person in the given range.
func GetTimeOffUsedForPerson(start string, end string, name string) (TimeOff, error) {
	counter := make(map[string]float64)
	conditions := fmt.Sprintf("timeStart>=[%s] AND timeStart<=[%s] AND chargeToId IN (2,3,4,32,43,50,52) AND member/identifier=\"%s\"", start, end, name)

	entries, err := getEntries(conditions)

	if err != nil {
		return TimeOff{}, err
	}

	for _, entry := range entries {
		typeName := getTimeOffCodeName(entry.ChargeCode)
		counter[typeName] += entry.Hours
	}

	return TimeOff{
		Name:         name,
		Vacation:     counter["Vacation"],
		PersonalTime: counter["Personal Time"],
		Holiday:      counter["Holiday"],
		Sick:         counter["Sick"],
		Jury:         counter["Jury"],
		Unpaid:       counter["Unpaid"],
		Bereavement:  counter["Bereavement"],
	}, nil

}

func getTimeOffCodeName(code int) string {
	codes := map[int]string{
		2:  "Vacation",
		3:  "Personal Time",
		4:  "Holiday",
		32: "Sick",
		43: "Jury",
		50: "Unpaid",
		52: "Bereavement",
	}

	return codes[code]
}

// GetTimeOff returns all the time off taken by all active team members in the given range.
func GetTimeOff(start string, end string) ([]TimeOff, error) {
	var list []TimeOff
	offChannel := make(chan TimeOff)
	people, err := person.GetActive(true)

	if err != nil {
		return []TimeOff{}, err
	}

	for _, person := range people {
		go func(s string, e string, n string) {
			off, err := GetTimeOffUsedForPerson(s, e, n)

			if err != nil {
				panic(err)
			}

			offChannel <- off
		}(start, end, person.Handle)

		listed := <-offChannel
		list = append(list, listed)
	}

	return list, nil
}
