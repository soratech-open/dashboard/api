package secret

import (
	"encoding/json"
	"os"
)

// Secret represents a collection of secret data
type Secret struct {
	Postgres    postgres    `json:"postgres"`
	Connectwise connectwise `json:"connectwise"`
	LDAP        ldap        `json:"ldap"`
	JWT         jwt         `json:"jwt"`
	Office      office      `json:"office"`
}

type postgres struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Schema   string `json:"schema"`
}

type connectwise struct {
	Token    string `json:"token"`
	ClientID string `json:"clientID"`
}

type ldap struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Address  string `json:"address"`
	Port     int    `json:"port"`
}

type jwt struct {
	Key string `json:"key"`
}

type office struct {
	User     string `json:"user"`
	Password string `json:"password"`
}

// Get returns the secret data as JSON.
func Get() (Secret, error) {
	serverEnv := os.Getenv("SERVER_ENVIRONMENT")
	var filename string
	var secrets Secret

	switch serverEnv {
	case "development":
		filename = "/var/www/statusboard/config/development.json"
	case "testing":
		filename = "/builds/soratech/Gamification/statusboard-api-2.0/src/secret/testData.json"
	}

	file, err := os.Open(filename)

	if err != nil {
		return secrets, err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&secrets)

	return secrets, err
}
