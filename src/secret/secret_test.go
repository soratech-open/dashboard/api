package secret

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGet(t *testing.T) {
	testData := Secret{
		Postgres: postgres{
			Host:     "test-database",
			Port:     5432,
			User:     "postgres",
			Password: "2SwRJNBI9MzK22dZ",
			Schema:   "statusboard",
		},
		Connectwise: connectwise{
			Token:    "thisIsATestToken",
			ClientID: "thisIsATestClientID",
		},
		LDAP: ldap{
			User:     "thisIsATestUser",
			Password: "thisIsATestPassword",
			Address:  "thisIsATestAddress",
			Port:     1234,
		},
		JWT: jwt{
			Key: "thisIsATestJWTKey",
		},
		Office: office{
			User:     "testUser",
			Password: "testEmailPassword",
		},
	}

	data, err := Get()

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(data, testData) {
		t.Fail()
	}
}
