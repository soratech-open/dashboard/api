package database

import (
	"database/sql"
	"fmt"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/secret"
	// Need this package to communicate with PostgreSQL
	_ "github.com/lib/pq"
)

// ConnectionString returns the postgres connection string.
func ConnectionString() string {
	secrets, err := secret.Get()

	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("host=%s port=%d user=%s password=%s sslmode=disable",
		secrets.Postgres.Host,
		secrets.Postgres.Port,
		secrets.Postgres.User,
		secrets.Postgres.Password,
	)
}

// RowExists queries the database to see if the given record exists.
func RowExists(query string, args ...interface{}) (bool, error) {
	var exists bool
	db, err := sql.Open("postgres", ConnectionString())

	if err != nil {
		return false, err
	}

	defer db.Close()

	query = fmt.Sprintf("SELECT EXISTS (%s)", query)

	err = db.QueryRow(query, args...).Scan(&exists)

	return exists, err
}

// Init initializes the database connection and returns a connection pool.
func Init() (*sql.DB, error) {
	db, err := sql.Open("postgres", ConnectionString())

	if err != nil {
		return &sql.DB{}, err
	}

	err = db.Ping()

	if err != nil {
		return &sql.DB{}, err
	}

	return db, nil
}
