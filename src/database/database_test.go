package database

import "testing"

func TestConnectionString(t *testing.T) {
	devString := "host=test-database port=5432 user=postgres password=2SwRJNBI9MzK22dZ sslmode=disable"

	connString := ConnectionString()

	if connString != devString {
		t.Error("Connection String incorrect")
	}
}

func TestRowExists(t *testing.T) {
	exists, err := RowExists("SELECT * FROM statusboard.person_types WHERE id = $1", 42)

	if err != nil {
		t.Error(err)
	}

	if exists {
		t.Fail()
	}
}
