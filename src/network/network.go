package network

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

// EnableCors enables CORS on http methods.
func EnableCors(w *http.ResponseWriter) {
	serverEnvironment := os.Getenv("SERVER_ENVIRONMENT")

	switch serverEnvironment {
	case "development":
		(*w).Header().Set("Access-Control-Allow-Origin", "http://localhost:32403")
	}

	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
}

// WriteJSON marshals the given Site struct into JSON and sends that JSON as an HTTP Response
func WriteJSON(w http.ResponseWriter, i interface{}) {
	u, err := json.Marshal(i)
	if err != nil {
		log.Print(err)
	}
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(u)
}
