package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/network"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/timeentry"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/project"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/authentication"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/metric"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/award"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/persontype"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/stats/current"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person/stats"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/person"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/ticket"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/connectwise/survey"
)

func main() {
	runServer()
}

func runServer() {
	mux := http.NewServeMux()

	mux.HandleFunc("/", index)
	mux.HandleFunc("/api/people/", peopleHandler)
	mux.HandleFunc("/api/current-stats/", currentStatsHandler)
	mux.HandleFunc("/api/stats/", statsHandler)
	mux.HandleFunc("/api/person-type", personTypeHandler)
	mux.HandleFunc("/api/award", awardHandler)
	mux.HandleFunc("/api/login", loginHandler)
	mux.HandleFunc("/api/project/", projectHandler)
	mux.HandleFunc("/api/tickets/count/", ticketCountHandler)
	mux.HandleFunc("/api/tickets/", ticketHandler)
	mux.HandleFunc("/api/surveys/", surveyHandler)
	mux.HandleFunc("/api/reports/", reportHandler)
	// mux.HandleFunc("/api/companies/", companyHandler)

	http.ListenAndServe(":80", mux)
}

func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Server is Running"))
}

// peopleHandler handles requests to /api/people/.
func peopleHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	params := r.URL.Query()
	handle := r.URL.Path[len("/api/people/"):]

	switch r.Method {
	case http.MethodGet:
		var list interface{}
		var err error

		switch handle {
		case "type":
			desiredType, _ := strconv.Atoi(params.Get("type"))
			list, err = person.GetByType(desiredType)
		case "active":
			active := true

			if params.Get("active") == "false" {
				active = false
			}

			list, err = person.GetActive(active)
		case "person/id":
			list, err = person.GetByID(params.Get("id"))
		case "person/handle":
			list, err = person.GetByHandle(params.Get("handle"))
		// case "person/time-off":
		// 	name := params.Get("handle")
		// 	now := time.Now()
		// 	currentYear := now.Year()
		// 	start := fmt.Sprintf("%d-01-01", currentYear)
		// 	end := fmt.Sprintf("%d-12-31", currentYear)

		// 	list, err = timeentry.GetTimeOffUsedForPerson(start, end, name)
		case "gotw":
			list, err = person.GetGeekOfTheWeek()
		default:
			list, err = person.GetAll()
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, list)
	case http.MethodPost:
		isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		if isAdmin {

			body, err := ioutil.ReadAll(r.Body)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			var newPerson person.Person
			err = json.Unmarshal(body, &newPerson)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(err.Error()))
				return
			}

			created, err := person.Create(newPerson)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(err.Error()))
				return
			}

			w.WriteHeader(http.StatusCreated)
			network.WriteJSON(w, created)
			return
		}

		w.WriteHeader(http.StatusForbidden)
	case http.MethodPut:
		isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		if isAdmin {
			body, err := ioutil.ReadAll(r.Body)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			var edit person.Person

			err = json.Unmarshal(body, &edit)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			edited, err := person.Update(edit)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			network.WriteJSON(w, edited)
			return
		}

		w.WriteHeader(http.StatusForbidden)
	case http.MethodPatch:
		isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		if isAdmin {
			body, err := ioutil.ReadAll(r.Body)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			var id string

			err = json.Unmarshal(body, &id)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			err = person.SetGeekOfTheWeek(id)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			w.WriteHeader(http.StatusNoContent)
			return
		}

		w.WriteHeader(http.StatusForbidden)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// statsHandler handles requests to /api/stats/:handle.
func statsHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	params := r.URL.Query()
	handle := r.URL.Path[len("/api/stats/"):]

	switch r.Method {
	case http.MethodGet:
		var data interface{}
		var err error
		switch handle {
		case "month":
			month, _ := strconv.Atoi(params.Get("month"))
			year, _ := strconv.Atoi(params.Get("year"))

			data, err = stats.GetForMonth(year, month)
		case "all":
			start := params.Get("start")
			end := params.Get("end")
			currentYear := time.Now().Year()

			if start == "" {
				start = fmt.Sprintf("%d-01-01", currentYear)
			}

			if end == "" {
				end = fmt.Sprintf("%d-12-31", currentYear)
			}

			data, err = stats.GetForDateRangeByPerson(start, end)
		case "person":
			currentYear := time.Now().Year()
			start := params.Get("start")
			end := params.Get("end")
			id := params.Get("id")

			if start == "" {
				start = fmt.Sprintf("%d-01-01", currentYear)
			}

			if end == "" {
				end = fmt.Sprintf("%d-12-31", currentYear)
			}

			data, err = stats.GetForPersonInRange(id, start, end)
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, data)
	case http.MethodPost:
		type statDates struct {
			Start string `json:"start"`
			End   string `json:"end"`
		}

		var dates statDates
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		if len(body) > 0 {
			err = json.Unmarshal(body, &dates)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}
		} else {
			now := time.Now()
			currentYear := now.Year()
			currentMonth := int(now.Month())
			nextMonth := int(now.AddDate(0, 1, 0).Month())
			firstOfCurrentMonth := fmt.Sprintf("%d-%d-01", currentYear, currentMonth)
			firstOfNextMonth := fmt.Sprintf("%d-%d-01", currentYear, nextMonth)

			dates.Start = firstOfCurrentMonth
			dates.End = firstOfNextMonth
		}

		data, err := metric.GetStatsInRange(dates.Start, dates.End)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		list, err := metric.MakeMonthStats(data)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		for _, stat := range list {
			_, err = stats.Add(stat)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}
		}
		w.WriteHeader(http.StatusNoContent)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// currentStatsHandler handles requests to /api/current-stats/:handle.
func currentStatsHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	params := r.URL.Query()
	handle := r.URL.Path[len("/api/current-stats/"):]

	switch r.Method {
	case http.MethodGet:
		var data interface{}
		var err error

		switch handle {
		case "person":
			id := params.Get("id")

			data, err = current.GetFor(id)
		case "type":
			personType, _ := strconv.Atoi(params.Get("type"))

			data, err = current.GetForType(personType)
		case "all":
			data, err = current.GetAll()
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, data)
	case http.MethodPost:
		now := time.Now()
		tomorrow := now.AddDate(0, 0, 1).Format("2006-01-02")
		sevenDaysAgo := now.AddDate(0, 0, -7).Format("2006-01-02")

		data, err := metric.GetStatsInRange(sevenDaysAgo, tomorrow)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		list, err := metric.MakeCurrentStats(data)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		for _, stat := range list {
			_, err = current.Add(stat)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}
		}
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// personTypeHandler handles requests to /api/person-type.
func personTypeHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	switch r.Method {
	case http.MethodGet:
		list, err := persontype.GetAll()

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, list)
	case http.MethodPost:
		isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		if isAdmin {
			var newType persontype.PersonType

			body, err := ioutil.ReadAll(r.Body)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			err = json.Unmarshal(body, &newType)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			created, err := persontype.Create(newType.Name)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			w.WriteHeader(http.StatusCreated)
			network.WriteJSON(w, created)
			return
		}

		w.WriteHeader(http.StatusForbidden)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// awardHandler handles requests to /api/award.
func awardHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	params := r.URL.Query()

	switch r.Method {
	case http.MethodGet:
		id := params.Get("id")

		if len(id) > 0 {
			list, err := award.GetFor(id)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			network.WriteJSON(w, list)
			return
		}

		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Missing id GET parameter"))
	case http.MethodPost:
		isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		if isAdmin {
			body, err := ioutil.ReadAll(r.Body)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			var newAward award.Award

			err = json.Unmarshal(body, &newAward)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			created, err := award.Create(newAward)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			switch newAward.Name {
			case "Geek of the Week":
				err = person.SetGeekOfTheWeek(newAward.Person)
			case "Geek of the Month":
				err = person.SetGeekOfTheMonth(newAward.Person)
			}

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			w.WriteHeader(http.StatusCreated)
			network.WriteJSON(w, created)
			return
		}

		w.WriteHeader(http.StatusForbidden)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	switch r.Method {
	case http.MethodPost:
		type loginData struct {
			Username string `json:"username"`
			Password string `json:"password"`
		}

		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		var data loginData

		err = json.Unmarshal(body, &data)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		userData, err := authentication.Login(data.Username, data.Password)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		http.SetCookie(w, &userData.Cookie)
		network.WriteJSON(w, userData.User)
	case http.MethodDelete:
		cookie := http.Cookie{
			Name:     "jwt",
			Value:    "",
			Path:     "/",
			Expires:  time.Unix(0, 0),
			Secure:   false,
			HttpOnly: true,
		}
		http.SetCookie(w, &cookie)
		w.WriteHeader(http.StatusNoContent)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// projectHandler handle requests to /api/project/:handle.
func projectHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	handle := r.URL.Path[len("/api/project/"):]
	params := r.URL.Query()

	switch r.Method {
	case http.MethodGet:
		var count int
		var err error
		switch handle {
		case "ytd":
			count, err = project.CountForCurrentYear()
		case "range":
			start := params.Get("start")
			end := params.Get("end")

			count, err = project.CountInRange(start, end)
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, count)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func ticketCountHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	handle := r.URL.Path[len("/api/tickets/count/"):]
	params := r.URL.Query()

	switch r.Method {
	case http.MethodGet:
		var counts interface{}
		var err error

		switch handle {
		case "person":
			name := params.Get("handle")
			start := params.Get("start")
			end := params.Get("end")
			now := time.Now()

			if start == "" {
				current := now.Format("2006-01")
				start = fmt.Sprintf("%s-01", current)
			}

			if end == "" {
				next := now.AddDate(0, 1, 0).Format("2006-01")
				end = fmt.Sprintf("%s-01", next)
			}

			counts, err = ticket.GetTotalClosedForPersonInRange(name, start, end)
		case "month":
			month := params.Get("month")

			if month == "" {
				month = time.Now().Format("2006-01")
			}

			counts, err = ticket.GetTicketsClosedForMonth(month)
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, counts)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func surveyHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)
	handle := r.URL.Path[len("/api/surveys/"):]
	params := r.URL.Query()
	isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		network.WriteJSON(w, err.Error())
		return
	}

	switch r.Method {
	case http.MethodGet:
		start := params.Get("start")
		end := params.Get("end")
		now := time.Now()
		var list []survey.Survey
		var err error

		if start == "" {
			start = now.AddDate(0, 0, -7).Format("2006-01-02")
		}

		if end == "" {
			end = now.Format("2006-01-02")
		}

		switch handle {
		case "person":
			name := params.Get("handle")

			isPerson, err := authentication.IsPerson(r, name)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				network.WriteJSON(w, err.Error())
				return
			}

			if isPerson || isAdmin {
				list, err = survey.GetForPersonInRange(name, start, end)
			} else {
				w.WriteHeader(http.StatusUnauthorized)
			}

		case "all":
			if isAdmin {
				list, err = survey.GetInRange(start, end)
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		case "bad":
			err = survey.BadEmail()
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, list)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func reportHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)
	handle := r.URL.Path[len("/api/reports/"):]
	params := r.URL.Query()

	switch r.Method {
	case http.MethodGet:
		var list interface{}
		var err error
		isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		switch handle {
		case "priorities":
			list, err = ticket.GetPriorities()
		case "total-tickets":
			if isAdmin {
				start := params.Get("start")
				end := params.Get("end")
				now := time.Now()

				if start == "" {
					start = now.AddDate(0, 0, -7).Format("2006-01-02")
				}

				if end == "" {
					end = now.Format("2006-01-02")
				}

				list = ticket.TotalTicketsInRange(start, end)
				err = nil
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		case "time-off":
			if isAdmin {
				start := params.Get("start")
				end := params.Get("end")
				currentYear := time.Now().Year()

				if start == "" {
					start = fmt.Sprintf("%d-01-01", currentYear)
				}

				if end == "" {
					end = fmt.Sprintf("%d-12-31", currentYear)
				}

				list, err = timeentry.GetTimeOff(start, end)
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, list)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// func companyHandler(w http.ResponseWriter, r *http.Request) {
// 	network.EnableCors(&w)

// 	handle := r.URL.Path[len("/api/companies/"):]
// 	params := r.URL.Query()

// 	switch r.Method {
// 	case http.MethodGet:
// 		var list interface{}
// 		var err error

// 		switch handle {
// 		case "status":
// 			status, err := strconv.Atoi(params.Get("status"))

// 			if err != nil {
// 				w.WriteHeader(http.StatusBadRequest)
// 				network.WriteJSON(w, err.Error())
// 				return
// 			}

// 			list, err = company.GetByStatus(status)
// 		case "id":
// 			id, err := strconv.Atoi(params.Get("id"))

// 			if err != nil {
// 				w.WriteHeader(http.StatusBadRequest)
// 				network.WriteJSON(w, err.Error())
// 				return
// 			}

// 			list, err = company.GetByID(id)
// 		case "identifier":
// 			list, err = company.GetByIdentifier(params.Get("code"))
// 		}

// 		if err != nil {
// 			w.WriteHeader(http.StatusBadRequest)
// 			network.WriteJSON(w, err.Error())
// 			return
// 		}

// 		network.WriteJSON(w, list)
// 	case http.MethodOptions:
// 		w.WriteHeader(http.StatusOK)
// 	default:
// 		w.WriteHeader(http.StatusMethodNotAllowed)
// 	}
// }

func ticketHandler(w http.ResponseWriter, r *http.Request) {
	network.EnableCors(&w)

	handle := r.URL.Path[len("/api/tickets/"):]
	params := r.URL.Query()
	isAdmin, err := authentication.HasAccess(r, "grpStatusBoardAdmin")

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		network.WriteJSON(w, err.Error())
		return
	}

	switch r.Method {
	case http.MethodGet:
		var list interface{}
		var err error

		switch handle {
		case "hourly/open":
			if isAdmin {
				list, err = ticket.GetOpenHourly()
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		case "hourly/closed":
			if isAdmin {
				start := params.Get("start")
				end := params.Get("end")
				now := time.Now()

				if start == "" {
					start = now.AddDate(0, 0, -7).Format("2006-01-02")
				}

				if end == "" {
					end = now.Format("2006-01-02")
				}

				list, err = ticket.GetClosedHourlyInRange(start, end)
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		case "average":
			start := params.Get("start")
			end := params.Get("end")
			now := time.Now()

			if start == "" {
				start = now.AddDate(0, 0, -7).Format("2006-01-02")
			}

			if end == "" {
				end = now.Format("2006-01-02")
			}

			list, err = ticket.GetAverageClosedInRange(start, end)
		case "same-day":
			start := params.Get("start")
			end := params.Get("end")
			now := time.Now()

			if start == "" {
				start = now.AddDate(0, 0, -7).Format("2006-01-02")
			}

			if end == "" {
				end = now.Format("2006-01-02")
			}

			list, err = ticket.GetSameDayCloseInRange(start, end)
		case "open-email":
			err = ticket.OpenTicketsEmail()
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			network.WriteJSON(w, err.Error())
			return
		}

		network.WriteJSON(w, list)
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
