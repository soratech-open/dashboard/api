package email

import (
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/smtp"
	"strings"

	"gitlab.com/soratech/Gamification/statusboard-api-2.0/secret"
)

// LoginAuth represents the credentials needed to login to the smtp server used for sending.
type LoginAuth struct {
	Username string
	Password string
}

// Email represents the email to be sent.
type Email struct {
	From    mail.Address
	To      mail.Address
	Cc      []mail.Address
	Bcc     []mail.Address
	Subject string
	Body    string
}

func makeAuth(username string, password string) smtp.Auth {
	return &LoginAuth{Username: username, Password: password}
}

// Start starts the SMTP session.
func (l *LoginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

// Next processes the next entry in the SMTP process.
func (l *LoginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(l.Username), nil
		case "Password:":
			return []byte(l.Password), nil
		default:
			return nil, errors.New("Unknown fromServer")
		}
	}

	return nil, nil
}

// Send sends the given email with the given login creds.
func Send(email Email) error {
	secrets, err := secret.Get()

	if err != nil {
		return err
	}
	auth := makeAuth(secrets.Office.User, secrets.Office.Password)
	headers := make(map[string]string)
	var ccString strings.Builder
	var bccString strings.Builder
	var message strings.Builder

	for _, address := range email.Cc {
		ccString.WriteString(address.Address)
		ccString.WriteString(", ")
	}

	for _, address := range email.Bcc {
		bccString.WriteString(address.Address)
		bccString.WriteString(", ")
	}

	headers["From"] = email.From.String()
	headers["To"] = email.To.String()
	headers["Cc"] = ccString.String()
	headers["Bcc"] = bccString.String()
	headers["Subject"] = email.Subject

	for k, v := range headers {
		message.WriteString(fmt.Sprintf("%s: %s\r\n", k, v))
	}

	message.WriteString(fmt.Sprintf("\r\n%s", email.Body))

	serverName := "smtp.office365.com:587"

	host, _, _ := net.SplitHostPort(serverName)

	tlsConfig := &tls.Config{
		InsecureSkipVerify: false,
		ServerName:         host,
	}

	connection, err := smtp.Dial(serverName)

	if err != nil {
		return err
	}

	connection.StartTLS(tlsConfig)

	err = connection.Auth(auth)

	if err != nil {
		return err
	}

	err = connection.Mail(email.From.Address)

	if err != nil {
		return err
	}

	err = connection.Rcpt(email.To.Address)

	if err != nil {
		return err
	}

	for _, address := range email.Cc {
		err = connection.Rcpt(address.Address)

		if err != nil {
			return err
		}
	}

	for _, address := range email.Bcc {
		err = connection.Rcpt(address.Address)

		if err != nil {
			return err
		}
	}

	w, err := connection.Data()

	if err != nil {
		return err
	}

	_, err = w.Write([]byte(message.String()))

	if err != nil {
		return err
	}

	err = w.Close()

	if err != nil {
		return err
	}

	connection.Quit()

	return nil
}

// SendHTML sends the given email as HTML with the given login creds.
func SendHTML(email Email) error {
	secrets, err := secret.Get()

	if err != nil {
		return err
	}

	auth := makeAuth(secrets.Office.User, secrets.Office.Password)
	headers := make(map[string]string)
	var ccString strings.Builder
	var bccString strings.Builder

	for _, address := range email.Cc {
		ccString.WriteString(address.Address)
		ccString.WriteString(", ")
	}

	for _, address := range email.Bcc {
		bccString.WriteString(address.Address)
		bccString.WriteString(", ")
	}

	headers["From"] = email.From.String()
	headers["To"] = email.To.String()
	headers["Cc"] = ccString.String()
	headers["Bcc"] = bccString.String()
	headers["Subject"] = email.Subject
	headers["Content-Type"] = "text/html;"

	var message strings.Builder

	for k, v := range headers {
		message.WriteString(fmt.Sprintf("%s: %s\r\n", k, v))
	}

	message.Write([]byte(fmt.Sprintf("\r\n%s", email.Body)))

	serverName := "smtp.office365.com:587"

	host, _, _ := net.SplitHostPort(serverName)

	tlsConfig := &tls.Config{
		InsecureSkipVerify: false,
		ServerName:         host,
	}

	connection, err := smtp.Dial(serverName)

	if err != nil {
		return err
	}

	connection.StartTLS(tlsConfig)

	err = connection.Auth(auth)

	if err != nil {
		return err
	}

	err = connection.Mail(email.From.Address)

	if err != nil {
		return err
	}

	err = connection.Rcpt(email.To.Address)

	if err != nil {
		return err
	}

	for _, address := range email.Cc {
		err = connection.Rcpt(address.Address)

		if err != nil {
			return err
		}
	}

	for _, address := range email.Bcc {
		err = connection.Rcpt(address.Address)

		if err != nil {
			return err
		}
	}

	w, err := connection.Data()

	if err != nil {
		return err
	}

	_, err = w.Write([]byte(message.String()))

	if err != nil {
		return err
	}

	err = w.Close()

	if err != nil {
		return err
	}

	connection.Quit()

	return nil
}
