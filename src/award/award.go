package award

import (
	"database/sql"
	"time"

	// Need this package to communicate with PostgreSQL
	_ "github.com/lib/pq"
	"gitlab.com/soratech/Gamification/statusboard-api-2.0/database"
)

// Award represents a single award in the database.
type Award struct {
	ID     int    `json:"id"`
	Person string `json:"person"`
	Name   string `json:"name"`
	Date   string `json:"date"`
}

// Create adds a new award to the database.
func Create(award Award) (Award, error) {
	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return Award{}, err
	}

	defer db.Close()

	award.Date = time.Now().Format("2006-01-02")

	row := db.QueryRow(`
		INSERT INTO statusboard.awards (person, name, date)
		VALUES ($1, $2, $3)
		RETURNING id;
	`, award.Person, award.Name, award.Date)

	err = row.Scan(&award.ID)

	return award, err
}

// GetFor returns all the awards earned by the given person.
func GetFor(person string) ([]Award, error) {
	var awards []Award

	db, err := sql.Open("postgres", database.ConnectionString())

	if err != nil {
		return []Award{}, err
	}

	defer db.Close()

	rows, err := db.Query("SELECT * FROM statusboard.awards WHERE person = $1 ORDER BY date DESC LIMIT 12", person)

	if err != nil {
		return []Award{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var scanned Award
		var scannedDate time.Time

		err = rows.Scan(
			&scanned.ID,
			&scanned.Person,
			&scanned.Name,
			&scannedDate,
		)

		if err != nil {
			return []Award{}, err
		}

		scanned.Date = scannedDate.Format("2006-01-02")

		awards = append(awards, scanned)
	}

	return awards, rows.Err()
}
