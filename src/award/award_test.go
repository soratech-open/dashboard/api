package award

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func makeTestData() []Award {
	return []Award{
		Award{
			ID:     2,
			Person: "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
			Name:   "Geek of the Week",
			Date:   "2019-09-13",
		},
		Award{
			ID:     1,
			Person: "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
			Name:   "Geek of the Week",
			Date:   "2019-06-10",
		},
	}
}

func TestGetFor(t *testing.T) {
	testData := makeTestData()

	awards, err := GetFor("f2c3bce4-dee2-11e9-8d5b-abb8311d612e")

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(awards, testData) {
		fmt.Printf("Expected: %v\n", testData)
		fmt.Printf("Results: %v\n", awards)
		t.Fail()
	}

}

func TestCreate(t *testing.T) {
	newAward := Award{
		ID:     3,
		Person: "f2c3bce4-dee2-11e9-8d5b-abb8311d612e",
		Name:   "Geek of the Week",
		Date:   time.Now().Format("2006-01-02"),
	}

	created, err := Create(newAward)

	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(created, newAward) {
		t.Fail()
	}
}
